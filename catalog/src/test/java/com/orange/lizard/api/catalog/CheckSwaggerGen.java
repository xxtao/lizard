package com.orange.lizard.api.catalog;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;

import java.io.FileWriter;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.boot.test.WebIntegrationTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.result.MockMvcResultHandlers;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import com.orange.lizard.catalog.LizardCatalogApplication;

@RunWith(SpringJUnit4ClassRunner.class)


//spring-boot-starter-test v1.6 ou ultérieur
@SpringApplicationConfiguration(classes = LizardCatalogApplication.class)
@WebIntegrationTest(value={"spring.profiles.active=native"},randomPort=true)                                                                                                                                             // ici, le port par defaut du serveur est surcharge. 0=ouvrir un port disponible.

public class CheckSwaggerGen {

	
    @Value("${local.server.port}")  //recuperation du port utilise par la Springboot Application
    private int serverRandomPort;

    @Autowired
    private WebApplicationContext context;

    
    private MockMvc mockMvc;

    @Before
    public void setUp() {
       this.mockMvc = MockMvcBuilders.webAppContextSetup(this.context).build();
    }

    
    
    @Test
    public void generateYmlDefinition() throws Exception{
    	this.mockMvc.perform(get("/v2/api-docs")
                .accept(MediaType.APPLICATION_JSON))
    			.andDo(MockMvcResultHandlers.print(new FileWriter("target/lizard-api-catalog.txt")));
    }
    
}
