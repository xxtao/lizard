package com.orange.lizard.catalog;

import static com.google.common.base.Predicates.or;
import static springfox.documentation.builders.PathSelectors.regex;

import java.io.IOException;
import java.lang.reflect.Type;
import java.net.URL;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

import javax.xml.datatype.XMLGregorianCalendar;

import org.apache.commons.io.IOUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.io.Resource;
import org.springframework.core.io.ResourceLoader;

import com.fasterxml.classmate.ResolvedType;
import com.fasterxml.classmate.TypeResolver;
import com.google.common.base.Predicate;

import io.swagger.models.Info;
import io.swagger.models.Path;
import io.swagger.models.Swagger;
import io.swagger.parser.SwaggerParser;
import springfox.documentation.builders.ApiInfoBuilder;
import springfox.documentation.schema.AlternateTypeRule;
import springfox.documentation.schema.WildcardType;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.service.Contact;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;

@Configuration
public class SwaggerConfig {
	
	private static final String SWAGGER_CATALOG_YAML = "classpath:/swagger/lizard-catalog-service.yaml";
	private static final Logger LOG = LoggerFactory.getLogger(SwaggerConfig.class);

	@Autowired
	ResourceLoader resloader;
	
    @Bean
    public Docket newsApi() {
    	Swagger api = lizardApiCatalog();
    	
        return new Docket(DocumentationType.SWAGGER_2)
                .apiInfo(apiInfo(api))
                .select()
                .paths(paths(api))
                .build()
                .forCodeGeneration(true)
//                .host(api.getHost())
               .alternateTypeRules(
                        getAlternateTypeRule(Collection.class, WildcardType.class, List.class,WildcardType.class)
                )     
               .directModelSubstitute(XMLGregorianCalendar.class, Date.class)
                ;
    }
     
	private Predicate<String> paths(Swagger api){
    	List<Predicate<String>> list = new ArrayList<>();
//    	for(String chemin: api.getPaths().keySet()){
//    		if(!chemin.contains("{")){//    	for(String chemin: api.getPaths().keySet()){
//		if(!chemin.contains("{")){
//		list.add(regex(chemin+".*"));
//	}
//}
//    			list.add(regex(chemin+".*"));
//    		}
//    	}
    	// Ces paths ne possèdent pas d'operation sans paramètre
     	 list.add(regex("/catalog.*"));
      	 list.add(regex("/category.*"));
      	 list.add(regex("/genericProduct.*"));
      	 list.add(regex("/productOffering.*"));
      	 list.add(regex("/characteristic.*"));
       	 list.add(regex("/eligibleCriteria.*"));
    	return or(list);
    }
    
    /**
     * API Info in swagger json/yml doesn't generate any Java artifact 
     * @return Informations concerning API.
     */
    private ApiInfo apiInfo(Swagger api) {
    	
        return new ApiInfoBuilder()
                .title(api.getInfo().getTitle())
                .description(api.getInfo().getDescription())
                .contact(new Contact("Orange-ISAD","",""))
                .version(api.getInfo().getVersion())                
                .build();
    }

    private Swagger lizardApiCatalog(){
    	Swagger swagger=null;
		try {
			String stringApi=null;
			if(null!=resloader){
				stringApi=IOUtils.toString(resloader.getResource(SWAGGER_CATALOG_YAML).getInputStream());
			} else {
				stringApi = IOUtils.toString(new URL(SWAGGER_CATALOG_YAML).openStream());
			}
			swagger= new SwaggerParser().parse(stringApi);
		} catch (IOException e) {
			LOG.warn("Error loading swagger specificiation: " +SWAGGER_CATALOG_YAML,e);
		}
		if(null==swagger){
			LOG.warn("Error parsing swagger");
			swagger = new Swagger();
			swagger.setInfo(new Info());
			swagger.setPaths(new HashMap<String,Path>());
		}
		return swagger;
    }
    
    
    public AlternateTypeRule getAlternateTypeRule(Type sourceType, Type sourceGenericType,
            Type targetType, Type targetGenericType) {
        TypeResolver typeResolver = new TypeResolver();
        ResolvedType source = typeResolver.resolve(sourceType, sourceGenericType);
        ResolvedType target = typeResolver.resolve(targetType, targetGenericType);
        return new AlternateTypeRule(source, target);
    }

    public AlternateTypeRule getAlternateTypeRule(Type sourceType, Type sourceGenericType,
            Type targetType) {
        TypeResolver typeResolver = new TypeResolver();
        ResolvedType source = typeResolver.resolve(sourceType, sourceGenericType);
        ResolvedType target = typeResolver.resolve(targetType);
        return new AlternateTypeRule(source, target);
    }    

    
}
