package com.orange.lizard.catalog.controller;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.francetelecom.soft.kernel.model.catalogue.Catalogue;
import com.francetelecom.soft.kernel.model.catalogue.RuleCriterionType;
import com.francetelecom.soft.kernel.service.catalogue.CatalogueService;
import com.orange.lizard.api.catalog.model.hal.EligibleCriteria;
import com.orange.lizard.api.catalog.model.hal.EligibleCriteria.TypeEnum;
import com.orange.lizard.api.catalog.resource.EligibleCriteriaApi;

import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

@Controller
@CrossOrigin
public class EligibleCriteriaApiController implements EligibleCriteriaApi {

	private static final String CRITERION_PREFIX = "order";
	//private static final String CRITERION_CHANNEL = "CHANNEL"

	@Autowired
	private CatalogueService catalogService;

	@ApiOperation(value = "list of eligibleCriterias as configured in Castor", notes = "Return list of configurationContextVariables including Channel (SCORING, KIND_OF_CLIENT, VIP, etc.). Used to configure BasketItem", response = EligibleCriteria.class, responseContainer = "List", tags = {}, nickname = "getEligibleCriterias")
	@ApiResponses(value = {
	        @ApiResponse(code = 200, message = "Success", response = EligibleCriteria.class) })
	@RequestMapping(value = "/eligibleCriteria", produces = { "application/json", "application/hal+json" }, method = RequestMethod.GET)
	@Override
	public ResponseEntity<List<EligibleCriteria>> getEligibleCriterias() {

		List<EligibleCriteria> result = new ArrayList<>();
		Collection<RuleCriterionType> ruleCriterionTypes = getCatalogue().getRuleCriterionTypes();
		if (null != ruleCriterionTypes) {
			for (RuleCriterionType ruleCriterionType : ruleCriterionTypes) {
				// GECKO: exclude channel criterion because it is manage in a
				// specific field in context data
				// LIZARD/EGECKO: Don't exclude Channel because we need to
				// retreive
				// possible values.
				String criterionCode = ruleCriterionType.getCode();
				if (ruleCriterionType.getValueExpressionPath().startsWith(CRITERION_PREFIX)) {
					// Lizard: Don't exclude channel &&
					// !CRITERION_CHANNEL.equals(criterionCode))
					EligibleCriteria ec = new EligibleCriteria();
					ec.setId(criterionCode);
					ec.setName(criterionCode);
					ec.setExpression(ruleCriterionType.getValueExpressionPath());
					if(null!=ruleCriterionType.getPossibleValues()){
						ec.setPossibleValue(ruleCriterionType.getPossibleValues());
					}
					if(!ec.getPossibleValue().isEmpty()){
						ec.setType(TypeEnum.LIST);
					} else {
						ec.setType(TypeEnum.valueOf(ruleCriterionType.getDataType().toString()));
					}
					result.add(ec);
				}
			}
		}
		return new ResponseEntity<>(result, HttpStatus.OK);
	}

	private Catalogue getCatalogue() {
		return catalogService.getCatalogue();
	}
}
