package com.orange.lizard.catalog.controller;

import java.util.ArrayList;
import java.util.List;

import com.francetelecom.soft.kernel.model.catalogue.FunctionValueSpecification;
import com.orange.lizard.api.catalog.model.hal.CharacteristicValue;
import com.orange.lizard.api.catalog.model.hal.ExternalIdentifier;

public class CharacteristicValueAssembler {//extends ResourceAssemblerSupport<FunctionValueSpecification,CharacteristicValue> {

	public CharacteristicValueAssembler(){
		// default constructor
	}
	

	public CharacteristicValue toResource(FunctionValueSpecification iconFv) {
		CharacteristicValue fv = new CharacteristicValue();
		fv.setId(iconFv.getCode());
		fv.setLabel(iconFv.getLabel());
		
		
		List<ExternalIdentifier> eis = new ArrayList<>();
		for(com.francetelecom.soft.kernel.model.catalogue.ExternalIdentifier iconEI : iconFv.getExternalIdentifiers()){
			ExternalIdentifier ei = new ExternalIdentifier();
			ei.setReferentialId(iconEI.getReferential());
			ei.setId(iconEI.getIdentifier());
			eis.add(ei);
		}
		fv.setExternalIdentifier(eis);
		return fv;
	}

}
