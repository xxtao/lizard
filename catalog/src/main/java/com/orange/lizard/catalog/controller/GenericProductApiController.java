package com.orange.lizard.catalog.controller;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.francetelecom.soft.kernel.model.catalogue.AbstractProductSpecification;
import com.francetelecom.soft.kernel.model.util.StringUtils;
import com.francetelecom.soft.kernel.service.catalogue.CatalogueService;
import com.orange.lizard.api.catalog.model.hal.GenericProduct;
import com.orange.lizard.api.catalog.resource.GenericProductApi;


@javax.annotation.Generated(value = "class io.swagger.codegen.languages.SpringCodegen", date = "2016-07-21T10:56:07.344+02:00")

@Controller
@CrossOrigin
public class GenericProductApiController implements GenericProductApi {

	
	@Autowired
	private CatalogueService catalogService;
	
	
	/**
	 * Je ne sais pas obtenir les GenericProducts d'un catalogue!!!
	 */
    @RequestMapping(value = "/genericProduct",
            produces = { "application/json", "application/hal+json" }, 
            method = RequestMethod.GET)
    @Override
    public ResponseEntity<List<GenericProduct>> genericProductsList() {
    	
    	List<String> genericProducts = new ArrayList<>();
    	for(AbstractProductSpecification pss : catalogService.getCatalogue().getProductSpecifications()){
    		String genProduct=pss.getGenericProduct();
    		if(StringUtils.isNotEmpty(genProduct) && ! genericProducts.contains(genProduct)){
    			genericProducts.add(genProduct);
    		}
    	}
    	
    	List<GenericProduct> result= new ArrayList<>();
    	for(String name : genericProducts){
    		GenericProduct prod = new  GenericProduct();
    		prod.setIdent(name);
    		prod.setName(name);
//    		prod.setDescription(name);
    		result.add(prod);
    	}
        return new ResponseEntity<List<GenericProduct>>(result,HttpStatus.OK);
    }

}
