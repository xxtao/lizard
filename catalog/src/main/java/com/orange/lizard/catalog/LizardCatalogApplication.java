package com.orange.lizard.catalog;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.hateoas.config.EnableHypermediaSupport;
import org.springframework.hateoas.config.EnableHypermediaSupport.HypermediaType;

import com.francetelecom.soft.kernel.service.catalogue.CatalogueService;
import com.francetelecom.soft.kernel.service.catalogue.CatalogueServiceImpl;

import springfox.documentation.swagger2.annotations.EnableSwagger2;


@Configuration
@ComponentScan
@EnableAutoConfiguration
@EnableHypermediaSupport(type={HypermediaType.HAL})
@EnableSwagger2
@EnableDiscoveryClient
public class LizardCatalogApplication {

	
	@Value("${lizard.catalogue.path:classpath:/catalog/}")
	private String catalogPath;
	
	
	@Bean
	public CatalogueService catalogueService()  {
		
		CatalogueServiceImpl catalogService = new CatalogueServiceImpl();
		catalogService.setGlobalCataloguePath(catalogPath);
		catalogService.setDelay(10);
		catalogService.setPeriod(5);
		catalogService.getFileLocator().setPastDateAttemptNumber(30);
		catalogService.getFileLocator().setFutureDateAttemptNumber(30);
		
		return catalogService;
		
	}

	public static void main(String[] args) {
		ConfigurableApplicationContext ctx = null;
		
		try {
			ctx = SpringApplication.run(LizardCatalogApplication.class, args);
		} catch (Exception e) {
			
			if (ctx != null) {
				ctx.close();
			}
		}
	}
}
