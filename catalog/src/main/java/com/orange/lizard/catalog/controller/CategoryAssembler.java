package com.orange.lizard.catalog.controller;

import static org.springframework.hateoas.mvc.ControllerLinkBuilder.linkTo;
import static org.springframework.hateoas.mvc.ControllerLinkBuilder.methodOn;

import java.util.ArrayList;
import java.util.List;

import org.springframework.hateoas.Link;
import org.springframework.hateoas.mvc.ResourceAssemblerSupport;

import com.francetelecom.soft.kernel.model.catalogue.Classification;
import com.orange.lizard.api.catalog.model.hal.Category;
import com.orange.lizard.api.catalog.model.hal.ExternalIdentifier;

public class CategoryAssembler extends ResourceAssemblerSupport<Classification,Category> {

	public CategoryAssembler(){
		super(CategoryApiController.class, Category.class);
	}
	public CategoryAssembler(Class<?> controllerClass, Class<Category> resourceType) {
		super(controllerClass, resourceType);
	}
	
	@Override
	public Category toResource(Classification entity) {
		Category categ = new Category();
		categ.setIdent(entity.getCode());
		categ.setIsRoot(entity.getClassificationType().getCode().equals("FAMILLE"));//entity.getParents().isEmpty());
		categ.setName(entity.getLabel());

		// ParentId uniquement si 1 seul parent
		if(entity.getParents().size()==1){
			for(Classification parent : entity.getParents()){
				categ.setParentId(parent.getCode());
			}
		}

		// Liens
		// ======
		// self 
		categ.add(linkTo(methodOn(CategoryApiController.class).categoryGet(categ.getIdent(),null)).withRel(Link.REL_SELF));
		// parent
		for(Classification parent : entity.getParents()){
			categ.add(linkTo(methodOn(CategoryApiController.class).categoryGet(parent.getCode(),null)).withRel("parent"));
		}
		// child
		for(Classification child : entity.getChildren()){
			categ.add(linkTo(methodOn(CategoryApiController.class).categoryGet(child.getCode(),null)).withRel("child"));
		}

		List<ExternalIdentifier> eis = new ArrayList<>();
		if(null!=entity.getExternalIdentifiers()){
			for(com.francetelecom.soft.kernel.model.catalogue.ExternalIdentifier iconEI : entity.getExternalIdentifiers()){
				ExternalIdentifier ei = new ExternalIdentifier();
				ei.setReferentialId(iconEI.getReferential());
				ei.setId(iconEI.getIdentifier());
				eis.add(ei);
			}
		}
		categ.setExternalIdentifier(eis);

		return categ;
	}

}
