package com.orange.lizard.catalog.utils;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;


public class CatalogUtils {

	
	public static Date toEffectiveDate(String catalogId){
		Date effectiveDate =null;
		try {
			effectiveDate =new SimpleDateFormat("yyyyMMdd").parse(catalogId);
//			effectiveDate =new SimpleDateFormat("yyyyMMdd").parse(Integer.toString(Integer.parseInt(catalogId) -1));
		} catch (ParseException e) {
		}
		return effectiveDate;
	}
	
	public static String toIdent(Date effectiveDate){
		return new SimpleDateFormat("yyyyMMdd").format(effectiveDate);
	}
	
}
