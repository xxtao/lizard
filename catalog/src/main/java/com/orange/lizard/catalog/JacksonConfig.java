package com.orange.lizard.catalog;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.converter.json.Jackson2ObjectMapperBuilder;

import com.fasterxml.jackson.annotation.JsonInclude;

@Configuration
public class JacksonConfig {
	
//	// expected single matching bean but found 2: OBJECT_MAPPER_BEAN,_halObjectMapper
//	@Bean(name = "OBJECT_MAPPER_BEAN")
//	@Primary
//	public ObjectMapper jsonObjectMapper() {
//	    return Jackson2ObjectMapperBuilder.json()
//	            .serializationInclusion(JsonInclude.Include.NON_EMPTY) // Don’t include null values
//	            .featuresToDisable(SerializationFeature.WRITE_DATES_AS_TIMESTAMPS) //ISODate
////	            .modules(new JavaTimeModule())
//	            .build();
//	}
//	

	
//	@Bean
//	public Jackson2ObjectMapperBuilder objectMapperBuilder() {
//	    Jackson2ObjectMapperBuilder builder = new Jackson2ObjectMapperBuilder();
//	    builder.serializationInclusion(JsonInclude.Include.NON_NULL);
//	    return builder;
//	}
	
	@Autowired(required = true)
	public void configeJackson(Jackson2ObjectMapperBuilder jackson2ObjectMapperBuilder) {
	    jackson2ObjectMapperBuilder.serializationInclusion(JsonInclude.Include.NON_EMPTY);
	}	
	// Il suffit de mettre la valeur dans le fichier application.properties :
	// spring.jackson.serialization.write_dates_as_timestamps=false
//	@Bean
//    @Primary
//    public ObjectMapper objectMapper(Jackson2ObjectMapperBuilder builder) {
//        ObjectMapper objectMapper = builder.createXmlMapper(false)
////        		.modules(new JavaTimeModule())
//        		.featuresToDisable(SerializationFeature.WRITE_DATES_AS_TIMESTAMPS)
//        		
//        		.build();
////        objectMapper.configure(SerializationFeature.WRITE_DATES_AS_TIMESTAMPS, false);
////        objectMapper.configure(SerializationFeature.WRITE_DATE_TIMESTAMPS_AS_NANOSECONDS, false);
//        SimpleDateFormat df = new SimpleDateFormat("dd-MM-yyyy");
//        objectMapper.setDateFormat(df);
//        objectMapper.findAndRegisterModules();
//        return objectMapper;
//    }
}
