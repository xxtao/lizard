package com.orange.lizard.catalog.controller;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import com.francetelecom.soft.kernel.model.catalogue.Catalogue;
import com.francetelecom.soft.kernel.model.catalogue.Classification;
import com.francetelecom.soft.kernel.service.catalogue.CatalogueService;
import com.orange.lizard.api.catalog.model.hal.Category;
import com.orange.lizard.api.catalog.resource.CategoryApi;

import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;


@javax.annotation.Generated(value = "class io.swagger.codegen.languages.SpringCodegen", date = "2016-07-21T10:56:07.344+02:00")

/**
 * Correspond aux classifications dans Castor
 * @author Orange Applications For Business
 *
 */
@Controller
@CrossOrigin
public class CategoryApiController implements CategoryApi {

	private static final Logger LOGGER = LoggerFactory.getLogger(CategoryApiController.class);
	
	// Seul les classifications de type FAMILLE nous intéresse
	private static final String FAMILLE="FAMILLE";
	
	@Autowired
	private CatalogueService catalogService;


	   @ApiOperation(value = "list categories", notes = "Retourne les categories", response = Category.class, responseContainer = "List", tags={  }, nickname="categoryList")
	    @ApiResponses(value = { 
	        @ApiResponse(code = 200, message = "Success", response = Category.class) })
	    @RequestMapping(value = "/category",
	        produces = { "application/json", "application/hal+json" }, 
	        method = RequestMethod.GET)
	@Override
    public ResponseEntity<List<Category>> categoryList(@ApiParam(value = "true par défaut - retourne uniquement les categories 'FAMILLE'") @RequestParam(value = "isRoot", required = false) Boolean isRoot,@ApiParam(value = "Selection du catalogue valide à cette date d''effet") @RequestParam(value = "catalogContractualDate", required = false) String catalogContractualDate){
    	
    	boolean filtreRoot = null==isRoot || isRoot.booleanValue();
    	
    	Catalogue cat = catalogService.getCatalogue(catalogContractualDate);

    	List<Category> result = new ArrayList<Category>();
    	
    	CategoryAssembler assembler = new CategoryAssembler();
    	
    	Collection<Classification> classifs;
    	if(filtreRoot){
    		classifs = cat.getClassificationsByType(FAMILLE);
    	} else {
    		classifs = cat.getClassifications();
    	}
    	for( Classification classif : classifs ){
			result.add(assembler.toResource(classif));
    	}
    	
        return new ResponseEntity<List<Category>>(result,HttpStatus.OK);
    }

    @ApiOperation(value = "get product category (classification)", notes = "", response = Category.class, tags={  }, nickname="categoryGet")
    @ApiResponses(value = { 
        @ApiResponse(code = 200, message = "Success", response = Category.class),
        @ApiResponse(code = 204, message = "No Content") })
    @RequestMapping(value = "/category/{categoryId}",
        produces = { "application/json", "application/hal+json" }, 
        method = RequestMethod.GET)
    @Override
	public ResponseEntity<Category> categoryGet(@ApiParam(value = "",required=true ) @PathVariable("categoryId") String categoryId,@ApiParam(value = "Selection du catalogue valide à cette date d''effet") @RequestParam(value = "catalogContractualDate", required = false) String catalogContractualDate) {
		if(StringUtils.isEmpty(categoryId)){
			return new ResponseEntity<Category>(HttpStatus.BAD_REQUEST);
		}
		
		Classification classif = catalogService.getCatalogue(catalogContractualDate).getClassification(categoryId);
		if(null==classif){
			return new ResponseEntity<Category>(HttpStatus.NO_CONTENT);
		}
		return new ResponseEntity<>(new CategoryAssembler().toResource(classif), HttpStatus.OK);
	}
}
