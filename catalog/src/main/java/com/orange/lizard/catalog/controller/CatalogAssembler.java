package com.orange.lizard.catalog.controller;

import static org.springframework.hateoas.mvc.ControllerLinkBuilder.linkTo;
import static org.springframework.hateoas.mvc.ControllerLinkBuilder.methodOn;

import org.joda.time.LocalDate;
import org.joda.time.format.DateTimeFormatter;
import org.joda.time.format.ISODateTimeFormat;
import org.springframework.hateoas.Link;
import org.springframework.hateoas.mvc.ResourceAssemblerSupport;

import com.francetelecom.soft.kernel.model.catalogue.Catalogue;
import com.orange.lizard.api.catalog.model.hal.Catalog;
import com.orange.lizard.catalog.utils.CatalogUtils;

public class CatalogAssembler extends ResourceAssemblerSupport<Catalogue, Catalog> {

	public CatalogAssembler() {
		super(CatalogApiController.class, Catalog.class);
	}

	public CatalogAssembler(Class<?> controllerClass, Class<Catalog> resourceType) {
		super(controllerClass, resourceType);

	}

	@Override
	public Catalog toResource(Catalogue iconCat) {
		Catalog c = new Catalog();

		c.setIdent(CatalogUtils.toIdent(iconCat.getEffectiveDate()));

		// adding links
		c.add(linkTo(methodOn(CatalogApiController.class).catalogGet(c.getIdent())).withRel(Link.REL_SELF));

		c.setName(iconCat.getCastorName());
		// c.setStartDate(LocalDate.fromDateFields(Instant.ofEpochMilli(iconCat.getEffectiveDate().getTime()).atZone(ZoneId.systemDefault()).toLocalDate());
		if (null != iconCat.getEffectiveDate()) {
			DateTimeFormatter fmt = ISODateTimeFormat.yearMonthDay();
			c.setStartDate(fmt.print(LocalDate.fromDateFields(iconCat.getEffectiveDate())));
		}
		// c.setEndDate(iconCat.getEffectiveDate());
		return c;
	}

}
