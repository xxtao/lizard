package com.orange.lizard.catalog;

import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.config.http.SessionCreationPolicy;

@Configuration
@EnableWebSecurity
public class SecurityConfig extends WebSecurityConfigurerAdapter {
    @Override
    protected void configure(AuthenticationManagerBuilder auth) throws Exception {
        auth.inMemoryAuthentication()
        .withUser("admin").password("admin").roles("ADMIN")
        .and()
        .withUser("egecko").password("motdepasse").roles("API");
                //.and()
                //.withUser("user2").password("secret2").roles("USER");
        
        //auth.ldapAuthentication();
        
        
    }
    @Override
    protected void configure(HttpSecurity http) throws Exception {
        http.authorizeRequests()
    	.antMatchers("/admin/**").access("hasRole('ROLE_ADMIN')")
    	.anyRequest().permitAll();// .access("hasRole('ROLE_USER')");
        http.httpBasic();
        http.csrf().disable();
        http.sessionManagement().sessionCreationPolicy(SessionCreationPolicy.STATELESS);
    }
}


