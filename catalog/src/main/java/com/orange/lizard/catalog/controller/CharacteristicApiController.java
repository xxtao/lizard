package com.orange.lizard.catalog.controller;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.francetelecom.soft.kernel.model.catalogue.Catalogue;
import com.francetelecom.soft.kernel.model.catalogue.FunctionSpecification;
import com.francetelecom.soft.kernel.service.catalogue.CatalogueService;
import com.orange.lizard.api.catalog.model.hal.Characteristic;
import com.orange.lizard.api.catalog.resource.CharacteristicApi;

import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

@javax.annotation.Generated(value = "class io.swagger.codegen.languages.SpringCodegen", date = "2016-07-21T10:56:07.344+02:00")

@Controller
@CrossOrigin
public class CharacteristicApiController implements CharacteristicApi {

	@Autowired
	private CatalogueService catalogService;

	@Override
    @ApiOperation(value = "get function", notes = "une caracteristique est une function d'un productSpecification", response = Characteristic.class, tags={  }, nickname="characteristicGet")
    @ApiResponses(value = { 
        @ApiResponse(code = 200, message = "Success", response = Characteristic.class),
        @ApiResponse(code = 204, message = "No Content") })
    @RequestMapping(value = "/characteristic/{characteristicId}",
        produces = { "application/json", "application/hal+json" }, 
        method = RequestMethod.GET)
    public ResponseEntity<Characteristic> characteristicGet(@ApiParam(value = "",required=true ) @PathVariable("characteristicId") String characteristicId){

    if (StringUtils.isEmpty(characteristicId)) {
			return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
		}

		Catalogue cat = catalogService.getCatalogue();

		FunctionSpecification iconFct = cat.getFunctionSpecification(characteristicId);
		if (null != iconFct) {
			Characteristic fct = new CharacteristicAssembler().toResource(iconFct);
			return new ResponseEntity<Characteristic>(fct, HttpStatus.OK);
		}
		return new ResponseEntity<Characteristic>(HttpStatus.NO_CONTENT);

	}

}
