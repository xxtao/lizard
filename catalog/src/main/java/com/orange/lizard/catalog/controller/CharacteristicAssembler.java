package com.orange.lizard.catalog.controller;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang3.StringUtils;
import org.springframework.hateoas.mvc.ResourceAssemblerSupport;

import com.francetelecom.soft.kernel.model.catalogue.AttributeType;
import com.francetelecom.soft.kernel.model.catalogue.FunctionSpecification;
import com.francetelecom.soft.kernel.model.catalogue.FunctionValueSpecification;
import com.orange.lizard.api.catalog.model.hal.Characteristic;
import com.orange.lizard.api.catalog.model.hal.Characteristic.ConfigurableEnum;
import com.orange.lizard.api.catalog.model.hal.Characteristic.PublicKeyTypeEnum;
import com.orange.lizard.api.catalog.model.hal.Characteristic.ValueTypeEnum;
import com.orange.lizard.api.catalog.model.hal.Characteristic.VisibleEnum;
import com.orange.lizard.api.catalog.model.hal.CharacteristicValue;
import com.orange.lizard.api.catalog.model.hal.ExternalIdentifier;

public class CharacteristicAssembler extends ResourceAssemblerSupport<FunctionSpecification,Characteristic> {

	public static final String PUBLICKEY_PROP="OC_IPKTYPE";
	private static final String DISPLAY_HIDE_IN_SYNTHESIS_PROPERTY = "DISPLAY_HIDE_IN_SYNTHESIS";
	private static final String DISPLAY_HIDE_IN_SYNTHESIS = "O";
	
	public CharacteristicAssembler(){
		super(CharacteristicApiController.class, Characteristic.class);
	}
	public CharacteristicAssembler(Class<?> controllerClass, Class<Characteristic> resourceType) {
		super(controllerClass, resourceType);
	}
	@Override
	public Characteristic toResource(FunctionSpecification entity) {
		Characteristic fct = new Characteristic();
		fct.setIdent(entity.getCode());
		fct.setConfigurable(AttributeType.editable.equals(entity.getType())? ConfigurableEnum.TRUE:ConfigurableEnum.FALSE);
		fct.setVisible(entity.isVisible() ? VisibleEnum.TRUE:VisibleEnum.FALSE);
		fct.setName(entity.getLabel());
		
		if(entity.getType()==AttributeType.editable){
			fct.setValueType(ValueTypeEnum.STRING);
			// TODO Déterminer si Date, Password, ...
		}
    	
		List<ExternalIdentifier> eis = new ArrayList<>();
		for(com.francetelecom.soft.kernel.model.catalogue.ExternalIdentifier iconEI : entity.getExternalIdentifiers()){
			ExternalIdentifier ei = new ExternalIdentifier();
			ei.setReferentialId(iconEI.getReferential());
			ei.setId(iconEI.getIdentifier());
			eis.add(ei);
		}
		fct.setExternalIdentifier(eis);
		
		List<CharacteristicValue> fvs = new ArrayList<>();
		CharacteristicValueAssembler fva = new CharacteristicValueAssembler();
		
		for(FunctionValueSpecification iconFv : entity.getValues()){
			CharacteristicValue fv = fva.toResource(iconFv);
			fvs.add(fv);
		}
		fct.setValue(fvs);
    	
    	String ipkType=entity.getPropertyValueByCode(PUBLICKEY_PROP);
    	if(null!=ipkType){
    		switch(ipkType.toUpperCase()){
    		case "MSISDN":
    			fct.setPublicKeyType(PublicKeyTypeEnum.MSISDN);
    			break;
    		case "ND":
    			fct.setPublicKeyType(PublicKeyTypeEnum.ND);
    			break;
    		case "ICC NUMBER":
    		case "ICC_NUMBER":
    			fct.setPublicKeyType(PublicKeyTypeEnum.ICC_NUMBER);
    			break;
    		default:
    			fct.setPublicKeyType(null);
    		}
    	}

    	// default (no output, show)
    	String hide=entity.getPropertyValueByCode(DISPLAY_HIDE_IN_SYNTHESIS_PROPERTY);
    	if(StringUtils.equals(hide, DISPLAY_HIDE_IN_SYNTHESIS)){
    		fct.setIsDisplayInSynthetisView(Boolean.FALSE);
    	}

		return fct;
	}

}
