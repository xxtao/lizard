package com.orange.lizard.catalog.controller;

import java.util.Arrays;
import java.util.Date;
import java.util.List;

import org.jsefa.common.converter.XMLGregorianCalendarConverter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import com.francetelecom.soft.kernel.model.catalogue.Catalogue;
import com.francetelecom.soft.kernel.service.catalogue.CatalogueService;
import com.orange.lizard.api.catalog.model.hal.Catalog;
import com.orange.lizard.api.catalog.resource.CatalogApi;
import com.orange.lizard.catalog.utils.CatalogUtils;

import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

@javax.annotation.Generated(value = "class io.swagger.codegen.languages.SpringCodegen", date = "2016-07-21T10:56:07.344+02:00")

@Controller
@CrossOrigin
public class CatalogApiController implements CatalogApi {

	@Autowired
	private CatalogueService catalogService;



    @ApiOperation(value = "list catalog", notes = "Retourne LE catalogue courant ou celui en vigueur à  une date donnée", response = Catalog.class, responseContainer = "List", tags={  }, nickname="catalogList")
    @ApiResponses(value = { 
        @ApiResponse(code = 200, message = "Success", response = Catalog.class) })
    @RequestMapping(value = "/catalog",
        produces = { "application/json", "application/hal+json" }, 
        method = RequestMethod.GET)
    public ResponseEntity<List<Catalog>> catalogList(@ApiParam(value = "Selection du catalogue valide à cette date d''effet") @RequestParam(value = "catalogContractualDate", required = false) String catalogContractualDate){


		Catalogue cat = null;
		if (null != catalogContractualDate) {
			cat = catalogService.getCatalogue(toDate(catalogContractualDate));
		} else {
			cat = catalogService.getCatalogue(toDate(catalogContractualDate));
		}
		if (null == cat) {
			return new ResponseEntity<List<Catalog>>(HttpStatus.NO_CONTENT);
		}

		HttpHeaders responseHeaders = new HttpHeaders();
		responseHeaders.set("new_version", "True");
		Catalog c = new CatalogAssembler().toResource(cat);
		return new ResponseEntity<List<Catalog>>(Arrays.asList(c), responseHeaders, HttpStatus.OK);
	}


    private Date toDate(String catalogContractualDate) {
		if(null==catalogContractualDate){
			return null;
		}
		return XMLGregorianCalendarConverter.create().fromString(catalogContractualDate).toGregorianCalendar().getTime();
		
	}


	@ApiOperation(value = "get catalog", notes = "", response = Catalog.class, tags={  }, nickname="catalogGet")
    @ApiResponses(value = { 
        @ApiResponse(code = 200, message = "Success", response = Catalog.class),
        @ApiResponse(code = 204, message = "No Content") })
    @RequestMapping(value = "/catalog/{catalogId}",
        produces = { "application/json", "application/hal+json" }, 
        method = RequestMethod.GET)
    public ResponseEntity<Catalog> catalogGet(@ApiParam(value = "",required=true ) @PathVariable("catalogId") String catalogId){


		Catalogue cat = getCatalog(catalogId);
		if (null == cat) {
			return new ResponseEntity<Catalog>(HttpStatus.NO_CONTENT);
		}
		Catalog c = new CatalogAssembler().toResource(getCatalog(catalogId));
		return new ResponseEntity<Catalog>(c, HttpStatus.OK);
	}

	/**
	 * @param catalogId
	 *            factultatif, retourne le catalogue courant si vide ou Empty.
	 * @return le catalog courant ou bien le catalog ayant exactement cette date
	 *         effective de commercialisation.
	 */
	private Catalogue getCatalog(String catalogId) {
		if (null == catalogId) {
			return catalogService.getCatalogue();
		}

		Date effectiveDate = CatalogUtils.toEffectiveDate(catalogId);
		if (null == effectiveDate) {
			return null;
		}
		Catalogue cat = catalogService.getCatalogue(effectiveDate);
		if (!effectiveDate.equals(cat.getEffectiveDate())) {
			cat = null;
		}
		return cat;
	}

}
