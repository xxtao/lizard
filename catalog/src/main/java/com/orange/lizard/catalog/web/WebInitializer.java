package com.orange.lizard.catalog.web;

import java.util.Properties;

import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.boot.context.web.SpringBootServletInitializer;

import com.orange.lizard.catalog.LizardCatalogApplication;

/**
 * Package SpringBoot application as WebApplication.
 * 
 * 
 * @author nstw7013
 *
 */
public class WebInitializer  extends SpringBootServletInitializer {
	
	@Override
	protected SpringApplicationBuilder configure(SpringApplicationBuilder application) {
		Properties defaultProperties = new Properties();
		String externalconfig = System.getProperty("lizard-api-catalog.configDirectory");
		if (null != externalconfig) {
			if (!externalconfig.endsWith("/") && !externalconfig.endsWith("\\")) {
				externalconfig = externalconfig + "/";
			}
			defaultProperties.put("spring.config.location", externalconfig);
			defaultProperties.put("logging.config", externalconfig + "logback.xml");
		}

		return application.properties(defaultProperties).sources(LizardCatalogApplication.class);
	}
}
