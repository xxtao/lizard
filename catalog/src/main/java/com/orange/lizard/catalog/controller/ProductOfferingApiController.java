package com.orange.lizard.catalog.controller;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.List;

import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import com.francetelecom.soft.kernel.model.catalogue.AbstractProductSpecification;
import com.francetelecom.soft.kernel.model.catalogue.AtomicOfferSpecification;
import com.francetelecom.soft.kernel.model.catalogue.Classification;
import com.francetelecom.soft.kernel.model.catalogue.OfferSpecification;
import com.francetelecom.soft.kernel.service.catalogue.CatalogueService;
import com.orange.lizard.api.catalog.model.hal.ProductOffering;
import com.orange.lizard.api.catalog.resource.ProductOfferingApi;

import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

@javax.annotation.Generated(value = "class io.swagger.codegen.languages.SpringCodegen", date = "2016-07-21T10:56:07.344+02:00")

@Controller
@CrossOrigin
public class ProductOfferingApiController implements ProductOfferingApi {

	private static final Logger LOGGER = LoggerFactory.getLogger(ProductOfferingApiController.class);

	@Autowired
	private CatalogueService catalogService;

	@ApiOperation(value = "Retourne les offres contenues dans une classification", notes = "Retourne les offres contenues dans une classification", response = ProductOffering.class, responseContainer = "List", tags = {}, nickname = "productOfferingList")
	@ApiResponses(value = {
	        @ApiResponse(code = 200, message = "Success", response = ProductOffering.class),
	        @ApiResponse(code = 204, message = "No Content") })
	@RequestMapping(value = "/productOffering", produces = { "application/json", "application/hal+json" }, method = RequestMethod.GET)
	@Override
	public ResponseEntity<List<ProductOffering>> productOfferingList(@ApiParam(value = "Classification, obligatoire si pas de recherche par id externe.") @RequestParam(value = "categoryId", required = false) String categoryId,
	        @ApiParam(value = "Liste des classifications commercialisables à une date donnée") @RequestParam(value = "catalogContractualDate", required = false) String catalogContractualDate,
	        @ApiParam(value = "referentiel externe dont les codes sont connus dans le catalog (eg BSCS)") @RequestParam(value = "externalReferentielId", required = false) String externalReferentielId,
	        @ApiParam(value = "valeur dans le referentiel externe") @RequestParam(value = "externalId", required = false) String externalId) {

		if (StringUtils.isBlank(externalReferentielId) && StringUtils.isNotBlank(externalId)) {
			LOGGER.warn("appel de productOfferingList avec un externalId={} mais sans referentialId", externalId);
			return new ResponseEntity<List<ProductOffering>>(HttpStatus.BAD_REQUEST);
		}
		if (StringUtils.isNotBlank(externalReferentielId) && StringUtils.isBlank(externalId)) {
			LOGGER.warn("appel de productOfferingList avec un externalReferentielId={} mais sans externalId", externalReferentielId);
			return new ResponseEntity<List<ProductOffering>>(HttpStatus.BAD_REQUEST);
		}

		if (StringUtils.isBlank(externalId) && StringUtils.isBlank(categoryId)) {
			LOGGER.warn("appel de productOfferingList sans critère de recherche");
			return new ResponseEntity<List<ProductOffering>>(HttpStatus.BAD_REQUEST);
		}

		List<OfferSpecification> filteredProducts;
		if (StringUtils.isNotBlank(externalId)) {
			filteredProducts = findByExternalId(externalReferentielId, externalId);
		} else {
			// Todo, convert catalogContractualDate
			filteredProducts = findByClassification(categoryId, null);
		}

		if (null == filteredProducts) {
			return new ResponseEntity<List<ProductOffering>>(HttpStatus.NO_CONTENT);
		}

		List<ProductOffering> result = new ArrayList<>();
		ProductOfferingAssember assembler = new ProductOfferingAssember();
		for (OfferSpecification os : filteredProducts) {
			ProductOffering prod = assembler.toResource(os, catalogContractualDate);
			result.add(prod);
		}
		return new ResponseEntity<List<ProductOffering>>(result, HttpStatus.OK);
	}

	private List<OfferSpecification> findByExternalId(String externalReferentielId, String externalId) {
		List<OfferSpecification> result = new ArrayList<>();

		for (AbstractProductSpecification ps : catalogService.getCatalogue().getProductSpecifications()) {
			if (ps instanceof AtomicOfferSpecification) {
				if (isExternalOffer(ps, externalReferentielId, externalId)) {
					result.add((OfferSpecification) ps);
				} else if (isExternalOffer(((AtomicOfferSpecification) ps).getProductSpecification(), externalReferentielId, externalId)) {
					result.add((OfferSpecification) ps);

				}
			} else if (ps instanceof OfferSpecification && isExternalOffer(ps, externalReferentielId, externalId)) {
				result.add((OfferSpecification) ps);
			}
		}
		return result;
	}

	private boolean isExternalOffer(AbstractProductSpecification ps, String externalReferentielId, String externalId) {
		boolean result = false;
		if(null==ps){
			return false;
		}
		Collection<String> coll = ps.getExternalIdentifiersForReferential(externalReferentielId);
		if (null != coll) {
			for (String ei : coll) {
				if (StringUtils.equalsIgnoreCase(externalId, ei)) {
					result = true;
					break;
				}
			}
		}
		return result;
	}

	/***
	 * 
	 * @param categoryId
	 * @return null = classification not found, empty or non empty
	 *         offerSpecification otherwise
	 */
	private List<OfferSpecification> findByClassification(String categoryId, Date catalogContractualDate) {
		Classification cl = catalogService.getCatalogue(catalogContractualDate).getClassification(categoryId);
		if (null == cl) {
			return null;
		}
		List<OfferSpecification> result = new ArrayList<>();
		for (AbstractProductSpecification ps : cl.getProducts()) {
			if (ps instanceof OfferSpecification) {
				result.add((OfferSpecification) ps);
			}
		}
		return result;
	}

	@ApiOperation(value = "get productOffering", notes = "", response = ProductOffering.class, tags = {}, nickname = "productOfferingGet")
	@ApiResponses(value = {
	        @ApiResponse(code = 200, message = "Success", response = ProductOffering.class),
	        @ApiResponse(code = 204, message = "No Content") })
	@RequestMapping(value = "/productOffering/{productOfferingId}", produces = { "application/json", "application/hal+json" }, method = RequestMethod.GET)
	@Override
	public ResponseEntity<ProductOffering> productOfferingGet(@ApiParam(value = "", required = true) @PathVariable("productOfferingId") String productOfferingId,
	        @ApiParam(value = "Selection du catalogue valide à cette date d''effet") @RequestParam(value = "catalogContractualDate", required = false) String catalogContractualDate) {

		if (StringUtils.isBlank(productOfferingId)) {
			return new ResponseEntity<ProductOffering>(HttpStatus.BAD_REQUEST);
		}

		OfferSpecification os = catalogService.getCatalogue(catalogContractualDate).getOfferSpecification(productOfferingId);
		if (null != os) {
			ProductOffering prod = new ProductOfferingAssember().toResource(os);

			return new ResponseEntity<ProductOffering>(prod, HttpStatus.OK);
		}
		return new ResponseEntity<ProductOffering>(HttpStatus.NO_CONTENT);
	}

}
