package com.orange.lizard.catalog.controller;

import static org.springframework.hateoas.mvc.ControllerLinkBuilder.linkTo;
import static org.springframework.hateoas.mvc.ControllerLinkBuilder.methodOn;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Set;

import org.apache.commons.lang.StringUtils;
import org.springframework.hateoas.Link;

import com.francetelecom.soft.kernel.model.catalogue.AtomicOfferSpecification;
import com.francetelecom.soft.kernel.model.catalogue.CatalogueProperty;
import com.francetelecom.soft.kernel.model.catalogue.Classification;
import com.francetelecom.soft.kernel.model.catalogue.CompositeOfferSpecification;
import com.francetelecom.soft.kernel.model.catalogue.FunctionSpecification;
import com.francetelecom.soft.kernel.model.catalogue.OfferSpecification;
import com.francetelecom.soft.kernel.model.catalogue.OfferSpecificationRole;
import com.francetelecom.soft.kernel.model.catalogue.ProductType;
import com.google.common.collect.HashMultimap;
import com.orange.lizard.api.catalog.model.hal.CatalogProperty;
import com.orange.lizard.api.catalog.model.hal.Characteristic;
import com.orange.lizard.api.catalog.model.hal.ExternalIdentifier;
import com.orange.lizard.api.catalog.model.hal.ProductOffering;
import com.orange.lizard.api.catalog.model.hal.ProductOffering.ProductOfferingTypeEnum;
import com.orange.lizard.api.catalog.model.hal.ProductOffering.VisibleEnum;
import com.orange.lizard.api.catalog.model.hal.ProductViewProperty;
import com.orange.lizard.api.catalog.model.hal.ProductViewProperty.DisplayModeEnum;

public class ProductOfferingAssember {//extends ResourceAssemblerSupport<OfferSpecification, ProductOffering> {

	private static final String PRODUCT_PROPERTY_REQUIRED_DOC = "requiredDoc";
	private static final String PRODUCT_PROPERTY_DISPLAY_HIDE_IN_SYNTHESIS = "DISPLAY_HIDE_IN_SYNTHESIS";
	private static final String PRODUCT_PROPERTY_DISPLAY_FOLDED = "DISPLAY_FOLDED_BY_DEFAULT";
	private static final String PRODUCT_PROPERTY_DISPLAY_TABLE = "OC_DISPMODE";
	private static final String DISPLAY_HIDE_IN_SYNTHESIS = "O";
	private static final String DISPLAY_FOLDED_VALUE="O"; 
	private static final String DISPLAY_TABLE_VALUE = "TABLE";
	private static final List<String> SPECIAL_PROPERTIES = Arrays.asList(PRODUCT_PROPERTY_DISPLAY_FOLDED, PRODUCT_PROPERTY_DISPLAY_HIDE_IN_SYNTHESIS, PRODUCT_PROPERTY_DISPLAY_TABLE);

	public ProductOfferingAssember() {
		// Default constructor
	}

	public ProductOffering toResource(OfferSpecification os) {
		return toResource(os,null);
	}
	
	public ProductOffering toResource(OfferSpecification os, String catalogContractualDate) {

		ProductOffering prod = new ProductOffering();
		prod.setId(os.getCode());
		prod.setName(os.getLabel());
		prod.setShortName(os.getShortLabel());
		if(StringUtils.isNotBlank(os.getDescription())){
		prod.setDescription(os.getDescription());
		}
		prod.setIsBundle(os instanceof CompositeOfferSpecification);
		prod.setIsTopLevel(os.getProductType() == ProductType.ContractType);

		prod.setVisible(os.isVisible() ? VisibleEnum.TRUE : VisibleEnum.FALSE);
		if(os.getProductType()==ProductType.ContractType){
			prod.setProductOfferingType(ProductOfferingTypeEnum.CONTRACT);
		} else {
			prod.setProductOfferingType(ProductOfferingTypeEnum.valueOf(os.getProductType().name().toUpperCase()));
		}
		prod.setGenericProduct(os.getGenericProduct());
		
		prod.setIsFulfillable(os.isFulfillable());
		prod.setIsInstallable(os.isInstallable());
		if(StringUtils.isNotBlank(os.getMarketingWeight())){
		prod.setMarketingWeight(os.getMarketingWeight());
		}
		
		prod.setHref(linkTo(methodOn(ProductOfferingApiController.class).productOfferingGet(prod.getId(), null)).withSelfRel().getHref());


		
		for(Classification cl : os.getClassifications()){
			String href=linkTo(methodOn(CategoryApiController.class).categoryGet(cl.getCode(), catalogContractualDate)).withRel("producCategory").getHref();
			com.orange.lizard.api.catalog.model.hal.Link lk=new com.orange.lizard.api.catalog.model.hal.Link();
			lk.setHref(href);
			lk.setName(cl.getLabel());
			prod.getCategory().add(lk);
		}
	
		// href
		prod.setHref(linkTo(methodOn(ProductOfferingApiController.class).productOfferingGet(prod.getId(),  catalogContractualDate))
				.withRel(Link.REL_SELF).getHref());

		// adding embedded resources
	
		switch (os.getProductType()) {
		case AtomicOffer:
			mapAtomicProduct(prod, (AtomicOfferSpecification) os);
			break;
		case ContractType:
		case Play:
		case Set:
			mapCompositProduct(prod, (CompositeOfferSpecification) os, catalogContractualDate);
			break;
		case ProductService:
		default:
			break;
		}

		List<ExternalIdentifier> eis = new ArrayList<>();
		for (com.francetelecom.soft.kernel.model.catalogue.ExternalIdentifier iconEI : os.getExternalIdentifiers()) {
			ExternalIdentifier ei = new ExternalIdentifier();
			ei.setReferentialId(iconEI.getReferential());
			ei.setId(iconEI.getIdentifier());
			eis.add(ei);
		}
		prod.setExternalIdentifier(eis);

		ProductViewProperty viewProperties = new ProductViewProperty();
		viewProperties.setIsFoldedByDefault(StringUtils.equals(getStringProperty(os,PRODUCT_PROPERTY_DISPLAY_FOLDED), DISPLAY_FOLDED_VALUE));
		viewProperties.setDisplayMode(StringUtils.equals(getStringProperty(os,PRODUCT_PROPERTY_DISPLAY_TABLE), DISPLAY_TABLE_VALUE) ? DisplayModeEnum.TABLE:null);
		viewProperties.setIsHideInSynthetisView(StringUtils.equals(getStringProperty(os,PRODUCT_PROPERTY_DISPLAY_HIDE_IN_SYNTHESIS), DISPLAY_HIDE_IN_SYNTHESIS));
		prod.setViewProperty(viewProperties);
		
		List<CatalogProperty> otherProps = new ArrayList<>();
		
		HashMultimap<String,String> mmMap = HashMultimap.create();
		
		for(CatalogueProperty cp : os.getProperties()){
			if(!cp.isDynamicValue() && !SPECIAL_PROPERTIES.contains(cp.getPropertyCode()) && !cp.isValueExpression()){
				mmMap.put(cp.getPropertyCode(), cp.getPropertyValue());
			}
		}
		 
		for( String key : mmMap.keySet()){
			CatalogProperty prop = new CatalogProperty();
			prop.setId(key);
			Set<String> set = mmMap.get(key);
			if(!set.isEmpty() && 1==set.size()){
				// Simple/mono-value
				prop.setSimpleValue(set.iterator().next());
			} else {
				// Multi-value
				prop.setMultiplesValues( new ArrayList<>(set));
			}
			otherProps.add(prop);
		}
		prod.setOtherProperty(otherProps);
		return prod;
	}

	private void mapCompositProduct(ProductOffering prod, CompositeOfferSpecification os,
			String catalogContractualDate) {

		List<com.orange.lizard.api.catalog.model.hal.Link> child = new ArrayList<>();
		for (OfferSpecificationRole role : os.getConfiguratorOfferRoles()) {
			if (role.getContainingOffer().equals(os)) {
				// Relation Parent/enfant
				com.orange.lizard.api.catalog.model.hal.Link lk =new com.orange.lizard.api.catalog.model.hal.Link();
				lk.setHref(linkTo(methodOn(ProductOfferingApiController.class)
						.productOfferingGet(role.getOffer().getCode(), catalogContractualDate )).withRel("child").getHref());
				lk.setName(role.getOffer().getLabel());
				child.add(lk);
			}
		}
		if(!child.isEmpty()){
			prod.setBundledProductOffering(child);
		}

	}

	private void mapAtomicProduct(ProductOffering prod, AtomicOfferSpecification os) {
		if (null == os.getProductSpecification()) {
			return;
		}

		prod.setProductSpecification(os.getProductSpecification().getCode());
		
		CharacteristicAssembler fa = new CharacteristicAssembler();

		List<Characteristic> fcts = new ArrayList<>();
		for (FunctionSpecification fs : os.getProductSpecification().getFunctions()) {
			fcts.add(fa.toResource(fs));
		}
		prod.setCharacteristic(fcts);
	}

	private String getStringProperty(OfferSpecification os, String propName){
		String value=os.getPropertyValueByCode(propName);
		return value==null?"":value;
	}
}
