package com.orange.lizard.catalog;

import javax.annotation.PreDestroy;
import javax.servlet.Filter;

import org.slf4j.LoggerFactory;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import com.orange.lizard.commons.logging.LoggingRequestFilter;
import com.orange.lizard.commons.logging.LoggingStreamFilter;

import ch.qos.logback.classic.LoggerContext;

@Configuration
public class LoggingConfig {

	/** Log Full message request/response
	 *  
	 *  com.orange.lizard.api.commons.logging.LoggingStreamFilter should not be redirect to console
	 */
	@Bean
	public Filter loggingFluxFilter() {
		return new LoggingStreamFilter();
	}

	/** Log 1 line per Request with response time and status
	 *  
	 *  com.orange.lizard.api.commons.logging.LoggingRequestFilter may be redirect to console
	 */
	@Bean
	public Filter loggingRequestFilter() {
		return new LoggingRequestFilter();
	}
	
	@Bean
	public LogbackShutdownHook hook(){
		return new LogbackShutdownHook();
	}

	/**
	 * Listening for Spring closing context to close Logback context
	 * 
	 * see :  http://logback.qos.ch/manual/configuration.html#stopContext
	 *
	 */
	private class LogbackShutdownHook{
		
		@PreDestroy
	    public void destroy() {
			if (LoggerFactory.getILoggerFactory() instanceof LoggerContext) {
		        System.out.println("Closing Logback context");
	            ((LoggerContext) LoggerFactory.getILoggerFactory()).stop();
	        }
	    }
	}
}
