<project xmlns="http://maven.apache.org/POM/4.0.0" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
	xsi:schemaLocation="http://maven.apache.org/POM/4.0.0 http://maven.apache.org/xsd/maven-4.0.0.xsd">
	<modelVersion>4.0.0</modelVersion>
	<groupId>com.orange.lizard</groupId>
	<artifactId>lizard-catalog-service</artifactId>
	<version>2.0.0-SNAPSHOT</version>

	<name>lizard-catalog-service</name>
	<description>Project for Lizard Catalog Service</description>
	<parent>
		<groupId>org.springframework.boot</groupId>
		<artifactId>spring-boot-starter-parent</artifactId>
		<version>1.4.6.RELEASE</version>
		<relativePath /> <!-- lookup parent from repository -->
	</parent>

	<properties>
		<project.build.sourceEncoding>UTF-8</project.build.sourceEncoding>
		<project.reporting.outputEncoding>UTF-8</project.reporting.outputEncoding>
		<java.version>1.8</java.version>
		<spring-cloud.version>Camden.RELEASE</spring-cloud.version>
		<swagger-core-version>1.5.9</swagger-core-version>
		<spring-kafka.version>1.2.0.RELEASE</spring-kafka.version>
		<lizard-catalog.version>2.0.0-SNAPSHOT</lizard-catalog.version>
		<lizard-commons.version>2.0.0-SNAPSHOT</lizard-commons.version>
		<spring-cloud-kubernetes.version>0.1.4</spring-cloud-kubernetes.version>
		<cxf.version>3.1.8</cxf.version>
		<softkernel.version>0.211</softkernel.version>
		<springfox.version>2.5.0</springfox.version>
		<swagger.codegen.version>2.2.1</swagger.codegen.version>
		<swagger-core-version>1.5.9</swagger-core-version>
		<swagger-parser.version>1.0.25</swagger-parser.version>
	</properties>
	
	<dependencies>

		<dependency>
			<groupId>${project.groupId}</groupId>
			<artifactId>lizard-common</artifactId>
			<version>${lizard-commons.version}</version>
		</dependency>

<!-- 		<dependency> -->
<!-- 			<groupId>${project.groupId}</groupId> -->
<!-- 			<artifactId>lizard-catalog-binary</artifactId> -->
<!-- 			<version>${lizard-catalog.version}</version> -->
<!-- 		</dependency> -->

		<dependency>
			<groupId>com.jayway.jsonpath</groupId>
			<artifactId>json-path</artifactId>
			<scope>test</scope>
		</dependency>
		<dependency>
			<groupId>org.springframework.boot</groupId>
			<artifactId>spring-boot-starter-test</artifactId>
			<scope>test</scope>
		</dependency>
		
		<dependency>
			<groupId>org.springframework.cloud</groupId>
			<artifactId>spring-cloud-starter-sleuth</artifactId>
		</dependency>
		
		<dependency>
			<groupId>org.springframework.cloud</groupId>
			<artifactId>spring-cloud-starter-zipkin</artifactId>
		</dependency>
		
		<dependency>
			<groupId>org.springframework.cloud</groupId>
			<artifactId>spring-cloud-starter-eureka</artifactId>
		</dependency>
		
		<!-- Spring-clous-config -->
		<dependency>
			<groupId>org.springframework.cloud</groupId>
			<artifactId>spring-cloud-config-server</artifactId>
		</dependency>

		<dependency>
			<groupId>org.springframework.boot</groupId>
			<artifactId>spring-boot-configuration-processor</artifactId>
			<optional>true</optional>
		</dependency>
		
<!-- 		<dependency> -->
<!-- 			<groupId>io.fabric8</groupId> -->
<!-- 			<artifactId>spring-cloud-starter-kubernetes</artifactId> -->
<!-- 			<version>${spring-cloud-kubernetes.version}</version> -->
<!-- 			<exclusions> -->
<!-- 				<exclusion> -->
<!-- 					<groupId>commons-logging</groupId> -->
<!-- 					<artifactId>commons-logging</artifactId> -->
<!-- 				</exclusion> -->
<!-- 			</exclusions> -->
<!-- 		</dependency> -->
		<dependency>
			<groupId>org.springframework.hateoas</groupId>
			<artifactId>spring-hateoas</artifactId>
		</dependency>

		<dependency>
			<groupId>org.springframework.plugin</groupId>
			<artifactId>spring-plugin-core</artifactId>
		</dependency>

		<dependency>
			<groupId>com.francetelecom.softkernel</groupId>
			<artifactId>soft-icon</artifactId>
			<version>${softkernel.version}</version>
			<exclusions>
				<exclusion>
					<groupId>org.slf4j</groupId>
					<artifactId>slf4j-log4j12</artifactId>
				</exclusion>
				<exclusion>
					<groupId>log4j</groupId>
					<artifactId>log4j</artifactId>
				</exclusion>
			</exclusions>
		</dependency>


		<dependency>
			<groupId>javax.servlet</groupId>
			<artifactId>javax.servlet-api</artifactId>
			<scope>provided</scope>
		</dependency>


		<!-- =================================== -->
		<!-- | SPRING-BOOT SECURITY | -->
		<!-- =================================== -->
		<dependency>
			<groupId>org.springframework.boot</groupId>
			<artifactId>spring-boot-starter-security</artifactId>
		</dependency>

		<!-- =================================== -->
		<!-- | SPRING-BOOT ACTUATOR | -->
		<!-- =================================== -->
		<dependency>
			<groupId>org.springframework.boot</groupId>
			<artifactId>spring-boot-starter-actuator</artifactId>
		</dependency>
		<dependency>
			<groupId>org.webjars</groupId>
			<artifactId>hal-browser</artifactId>
		</dependency>


		<!-- =================================== -->
		<!-- | SWAGGER | -->
		<!-- =================================== -->
		<!-- Integrate Swagger-ui : /swagger-ui.html -->
		<dependency>
			<groupId>io.springfox</groupId>
			<artifactId>springfox-swagger-ui</artifactId>
			<version>${springfox.version}</version>
			<scope>compile</scope>
		</dependency>

		<!-- Expose API definition as Swagger/Json : /v2/apidocs -->
		<dependency>
			<groupId>io.springfox</groupId>
			<artifactId>springfox-swagger2</artifactId>
			<version>${springfox.version}</version>
			<scope>compile</scope>
		</dependency>
		<dependency>
			<groupId>io.springfox</groupId>
			<artifactId>springfox-staticdocs</artifactId>
			<version>${springfox.version}</version>
		</dependency>

		<!-- Read Swagger definition to extract an expose API définition -->
		<dependency>
			<groupId>io.swagger</groupId>
			<artifactId>swagger-parser</artifactId>
			<version>${swagger-parser.version}</version>
		</dependency>
		<dependency>
			<!-- Swagger-parser need swagger Version 1.5.8 and not 1.5.3 -->
			<groupId>io.swagger</groupId>
			<artifactId>swagger-core</artifactId>
			<version>${swagger-core-version}</version>
		</dependency>
		<dependency>
			<groupId>io.swagger</groupId>
			<artifactId>swagger-annotations</artifactId>
			<version>${swagger-core-version}</version>
		</dependency>
		<dependency>
			<!-- Swagger-parser need org/apache/commons/lang3/Validate -->
			<groupId>org.apache.commons</groupId>
			<artifactId>commons-lang3</artifactId>
			<version>3.4</version>
		</dependency>

		<!-- =================================== -->
		<!-- | TEST Dependencies | -->
		<!-- =================================== -->

		<!-- RestAssured -->
		<!-- <dependency> <groupId>com.jayway.restassured</groupId> <artifactId>rest-assured</artifactId> 
			</dependency> -->
		<!-- <dependency> <groupId>io.springfox</groupId> <artifactId>springfox-staticdocs</artifactId> 
			<scope>test</scope> </dependency> -->
	</dependencies>
	
	<dependencyManagement>
		<dependencies>
			<dependency>
				<groupId>org.springframework.cloud</groupId>
				<artifactId>spring-cloud-dependencies</artifactId>
				<version>${spring-cloud.version}</version>
				<type>pom</type>
				<scope>import</scope>
			</dependency>
		</dependencies>
	</dependencyManagement>
	
	<distributionManagement>
		<repository>
			<id>inhouse</id>
			<name>inhouse repository</name>
			<uniqueVersion>true</uniqueVersion>
			<url>http://maven2.rd.francetelecom.fr/proxy/repository/inhouse</url>
			<layout>default</layout>
		</repository>
		<snapshotRepository>
			<id>inhouse.snapshot</id>
			<name>inhouse.snapshot repository</name>
			<uniqueVersion>false</uniqueVersion>
			<url>http://maven2.rd.francetelecom.fr/proxy/repository/inhouse.snapshot</url>
			<layout>default</layout>
		</snapshotRepository>
	</distributionManagement>
	
	<repositories>

		<!-- Include soft-icon and api-bscs artifacts -->
		<repository>
			<id>project-local-repo</id>
			<url>file://${basedir}/../maven-repo</url>
		</repository>


		<!-- snapshots repositories -->
		<repository>
			<id>inhouse.snapshot</id>
			<name>inhouse.snapshot repository</name>
			<url>http://maven2.rd.francetelecom.fr/proxy/repository/inhouse.snapshot</url>
			<releases>
				<enabled>false</enabled>
			</releases>
			<snapshots>
				<enabled>true</enabled>
			</snapshots>
		</repository>
		<repository>
			<id>apache.snapshot</id>
			<name>apache.snapshot repository</name>
			<url>http://maven2.rd.francetelecom.fr/proxy/repository/apache.snapshot</url>
			<releases>
				<enabled>false</enabled>
			</releases>
			<snapshots>
				<enabled>true</enabled>
			</snapshots>
		</repository>
		<repository>
			<id>codehaus.snapshot</id>
			<name>codehause.snapshot repository</name>
			<url>http://maven2.rd.francetelecom.fr/proxy/repository/codehaus.snapshot</url>
			<releases>
				<enabled>false</enabled>
			</releases>
			<snapshots>
				<enabled>true</enabled>
			</snapshots>
		</repository>

		<!-- release repositories -->
		<repository>
			<id>inhouse</id>
			<name>inhouse repository</name>
			<url>http://maven2.rd.francetelecom.fr/proxy/repository/inhouse</url>
			<releases>
				<enabled>true</enabled>
			</releases>
			<snapshots>
				<enabled>false</enabled>
			</snapshots>
		</repository>
		<repository>
			<id>public</id>
			<name>public repository (corporate, extFree, extNonFree, Central)</name>
			<url>http://maven2.rd.francetelecom.fr/proxy/repository/public</url>
			<releases>
				<enabled>true</enabled>
			</releases>
			<snapshots>
				<enabled>false</enabled>
			</snapshots>
		</repository>
		<repository>
			<id>codehaus</id>
			<name>codehaus repository</name>
			<url>http://maven2.rd.francetelecom.fr/proxy/repository/codehaus</url>
			<releases>
				<enabled>true</enabled>
			</releases>
			<snapshots>
				<enabled>false</enabled>
			</snapshots>
		</repository>
		<repository>
			<id>maven.dev.java.net</id>
			<name>dev.java.net librairies maven repository</name>
			<url>http://maven2.rd.francetelecom.fr/proxy/shadows/maven.dev.java.net</url>
			<releases>
				<enabled>true</enabled>
			</releases>
			<snapshots>
				<enabled>false</enabled>
			</snapshots>
		</repository>
		<repository>
			<id>maven2.dev.java.net</id>
			<name>dev.java.net librairies maven2 repository</name>
			<url>http://maven2.rd.francetelecom.fr/proxy/repository/maven2.dev.java.net</url>
			<releases>
				<enabled>true</enabled>
			</releases>
			<snapshots>
				<enabled>false</enabled>
			</snapshots>
		</repository>
		<repository>
			<id>exhouse</id>
			<name>exhouse repository</name>
			<url>https://repo1.maven.org/maven2</url>
			<releases>
				<enabled>true</enabled>
			</releases>
			<snapshots>
				<enabled>false</enabled>
			</snapshots>
		</repository>
	</repositories>

	<build>
<!-- 		<finalName>${finalName}</finalName> -->
		<pluginManagement>

			<plugins>
				<plugin>
					<groupId>io.swagger</groupId>
					<artifactId>swagger-codegen-maven-plugin</artifactId>
					<version>${swagger.codegen.version}</version>
				</plugin>
			</plugins>
		</pluginManagement>

		<plugins>
			<plugin>
				<groupId>org.springframework.boot</groupId>
				<artifactId>spring-boot-maven-plugin</artifactId>
			</plugin>

			<!-- Generate Classes from json/yml Swagger definition WARN: Use a custom 
				codegen able to generate SpringAteaos Model -->
			<plugin>
				<groupId>io.swagger</groupId>
				<artifactId>swagger-codegen-maven-plugin</artifactId>
				<executions>
					<execution>
						<id>generate-swagger-model</id>
						<phase>generate-sources</phase>
						<goals>
							<goal>generate</goal>
						</goals>
						<configuration>
							<inputSpec>${project.basedir}/src/main/resources/swagger/${project.artifactId}.yaml</inputSpec>
							<language>lizard</language>
							<modelPackage>${project.groupId}.api.catalog.model.hal</modelPackage>
							<apiPackage>${project.groupId}.api.catalog.resource</apiPackage>
							<invokerPackage>${project.groupId}.api.catalog.client.invoker</invokerPackage>
							<configOptions>
								<dateLibrary>legacy</dateLibrary>
							</configOptions>
						</configuration>
					</execution>
				</executions>
				<dependencies>
					<dependency>
						<groupId>com.orange.lizard</groupId>
						<artifactId>lizard-codegen-swagger</artifactId>
						<version>2.0.0-SNAPSHOT</version>
					</dependency>
				</dependencies>
			</plugin>
			<plugin>
				<groupId>org.codehaus.mojo</groupId>
				<artifactId>build-helper-maven-plugin</artifactId>
				<executions>

					<execution>
						<id>add-generated-source</id>
						<phase>initialize</phase>
						<goals>
							<goal>add-source</goal>
						</goals>
						<configuration>
							<sources>
								<source>${generated-sources-path}/${generated-sources-java-path}</source>
							</sources>
						</configuration>
					</execution>
				</executions>
			</plugin>

		</plugins>
	</build>


	<profiles>
		<profile>
			<id>docker</id>
			<build>
				<plugins>
					<plugin>
						<groupId>com.spotify</groupId>
						<artifactId>docker-maven-plugin</artifactId>
						<configuration>
							<skipDocker>false</skipDocker>
						</configuration>
					</plugin>
				</plugins>
			</build>
		</profile>

		<profile>
			<id>openshift</id>
			<properties>
				<finalName>catalog</finalName>
			</properties>
			<build>
				<plugins>
					<plugin>
						<groupId>org.apache.maven.plugins</groupId>
						<artifactId>maven-dependency-plugin</artifactId>
						<configuration>
							<skip>false</skip>
						</configuration>
					</plugin>
				</plugins>
			</build>
		</profile>
	</profiles>
</project>
