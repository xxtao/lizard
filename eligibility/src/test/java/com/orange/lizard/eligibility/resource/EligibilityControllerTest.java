package com.orange.lizard.eligibility.resource;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collection;
import java.util.Date;
import java.util.List;

import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.IntegrationTest;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.boot.test.TestRestTemplate;
import org.springframework.boot.test.WebIntegrationTest;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.web.client.RestTemplate;

import com.francetelecom.soft.kernel.model.catalogue.Catalogue;
import com.francetelecom.soft.kernel.model.catalogue.Classification;
import com.francetelecom.soft.kernel.model.catalogue.CommercialParameterSpecification;
import com.francetelecom.soft.kernel.model.catalogue.ContractTypeSpecification;
import com.francetelecom.soft.kernel.model.catalogue.OfferSpecification;
import com.francetelecom.soft.kernel.model.customerorder.CustomerOrder;
import com.francetelecom.soft.kernel.model.customerorder.CustomerOrderCompositeItem;
import com.francetelecom.soft.kernel.model.customerorder.CustomerOrderItem;
import com.francetelecom.soft.kernel.model.eligibility.EligibleOfferAction;
import com.francetelecom.soft.kernel.model.installedbase.InstalledCompositeOffer;
import com.francetelecom.soft.kernel.model.installedbase.InstalledOffer;
import com.francetelecom.soft.kernel.service.catalogue.CatalogueService;
import com.francetelecom.soft.kernel.service.eligibility.ContractAcquisitionEligibilityRequest;
import com.francetelecom.soft.kernel.service.eligibility.EligibilityRequest;
import com.francetelecom.soft.kernel.service.eligibility.EligibilityRequestContext;
import com.francetelecom.soft.kernel.service.eligibility.EligibilityRequestContract;
import com.francetelecom.soft.kernel.service.eligibility.EligibilityService;
import com.francetelecom.soft.kernel.service.eligibility.EligibleOffer;
import com.francetelecom.soft.kernel.service.eligibility.EligibleOffers;
import com.francetelecom.soft.kernel.service.eligibility.MigrationEligibilityRequest;
import com.orange.lizard.consumer.productinventory.bscs.v4.rest.restModel.Product;
import com.orange.lizard.consumer.productinventory.bscs.v4.rest.restModel.Products;
import com.orange.lizard.eligibility.LizardEligibilityApplication;
import com.orange.lizard.eligibility.model.CategoryIcon;
import com.orange.lizard.eligibility.model.ProductOfferingIcon;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = LizardEligibilityApplication.class)
@IntegrationTest("server.port:0")
@WebIntegrationTest({ "spring.profiles.active=native",
		"spring.cloud.config.server.native.searchLocations=classpath:/operationCommercialeWithoutParcElement.properties","lizard.catalogue.path=classpath:/tmp/catalog" })
public class EligibilityControllerTest {

	private static final Logger log = LoggerFactory.getLogger(EligibilityControllerTest.class);

	@Autowired
	private EligibilityService eligibilityService;

	@Autowired
	private CatalogueService catalogueService;
	
	@Value("${productInventoryAPI}")
	private String urProductInventoryAPI;

	@Test
	public void testGetListEligibleOffersForAcquisition() {

		String categoryRequest = "Offres Jordanie";

		Catalogue catalogue;
		CustomerOrder customerOrder = new CustomerOrder();

		Date virtualDate = Calendar.getInstance().getTime();

		catalogue = getCatalogue(virtualDate);
		if (virtualDate != null) {
			customerOrder.setCreationDate(virtualDate);
			customerOrder.setConfigurationDate(virtualDate);

		} else {
			customerOrder.setConfigurationDate(catalogue.getEffectiveDate());
		}

		
		// productInventoryId CONTR0000001466
		customerOrder.setCatalogueVersion(catalogue.getVersion());
		customerOrder.setDistributorChannelId("PHONE");
		Collection<Classification> allClass = catalogue.getClassifications();
		Collection<Classification> classifEligibility = new ArrayList<Classification>();

		for (Classification mytest : allClass) {
			if ((mytest.getLabel().equalsIgnoreCase(categoryRequest)) && (!mytest.getParents().isEmpty())) {
				while (!mytest.getParents().isEmpty()) {
					for (Classification parent : mytest.getParents()) {
						for (Classification fils : parent.getChildren()) {
							classifEligibility.add(fils);
						}
					}
				}

			} else if ((mytest.getLabel().equalsIgnoreCase(categoryRequest)) && (mytest.getParents().isEmpty())) {
				classifEligibility.add(mytest);
			}

		}
		log.info("taille table catégorie" + classifEligibility.size());

		List<ProductOfferingIcon> res = new ArrayList<ProductOfferingIcon>();

		List<EligibleOffer> listOfEligibleOffersForCategory = new ArrayList<EligibleOffer>();

//		Collection<CustomerOrderItem> customerOrderItems = customerOrder.getCustomerOrderItems();

		// *********************** operation Commerciale
		Collection<EligibleOfferAction> eligibleOfferAction2 = eligibilityService
				.getEligibleActionsForAcquisition(customerOrder, new String[] {});
		for (EligibleOfferAction eligibleOfferAction : eligibleOfferAction2) {

			log.info(eligibleOfferAction.getAction().getActionType().toString());
		}

		// *********************** operation Commerciale

		ContractAcquisitionEligibilityRequest acqRequest;
		
		acqRequest = ContractAcquisitionEligibilityRequest.build()
				.withContext(EligibilityRequestContext.build(customerOrder).create()).create();
//		List<InstalledCompositeOffer> test = acqRequest.getContext().getInstalledContractOffers();
		EligibleOffers foundOffers = eligibilityService.getEligibleContractsForAcquisition(acqRequest);

//		ActionType[] testact = ActionType.values();

		log.info(foundOffers.toString());
		log.info(foundOffers.getEligibleOffers().toString());
		List<EligibleOffer> listOfEligibleOffers = new ArrayList<EligibleOffer>();
		listOfEligibleOffers = foundOffers.getEligibleOffers();

//		Collection<Classification> classifications = new ArrayList<Classification>();

//		log.info("taille 1 = " + listOfEligibleOffers.size());

		for (EligibleOffer eligibleOffer : listOfEligibleOffers) {

			EligibilityRequest eliRequest;
			// contract code : il faut un code contrat et pas un contractid

			EligibilityRequestContract eligibilityRequestContract = EligibilityRequestContract.build()
					.withContractCode("BSSCO001").create();

			// contrat BSSCO001 Talk Now offres B2B CL0000000005
			eliRequest = EligibilityRequest.build().withContract(eligibilityRequestContract)
					.withContext(EligibilityRequestContext.build(customerOrder).create()).withOfferCode("BSSAO001")
					.create();

			Collection<EligibleOfferAction> eligibleOfferAction1 = eligibilityService
					.getEligibleActionsForAcquisition(customerOrder, null);
			for (EligibleOfferAction eligibleOfferActionItem : eligibleOfferAction1) {
				log.info(eligibleOfferActionItem.getAction().getActionType().toString());
				log.info("isSingleton" + eligibleOfferActionItem.getAction().getActionType().isSingleton());
				log.info(eligibleOfferActionItem.getAction().getActionType().name().toString());
			}
			Collection<EligibleOfferAction> eligibleOfferAction = eligibilityService.getEligibleActions(eliRequest,
					null);
			for (EligibleOfferAction eligibleOfferActionItem : eligibleOfferAction) {
				log.info(eligibleOfferActionItem.getAction().getActionType().toString());
				log.info("isSingleton" + eligibleOfferActionItem.getAction().getActionType().isSingleton());
				log.info(eligibleOfferActionItem.getAction().getActionType().name().toString());
			}

			Collection<CommercialParameterSpecification> commercialParameterSpecification = eligibleOffer
					.getOfferSpecification().getCommercialParameters();

			ProductOfferingIcon product = new ProductOfferingIcon();
			// log.info(eligibleOffer.toString());
			OfferSpecification offerSpecification = eligibleOffer.getOfferSpecification();
			log.info(offerSpecification.getGenericProduct());
			product.setGenericProduct(offerSpecification.getGenericProduct());
			log.info(offerSpecification.getCode());
			product.setId(offerSpecification.getCode());
			log.info(offerSpecification.getLabel());
			product.setName(offerSpecification.getLabel());

//			classifications = offerSpecification.getClassifications();

			CategoryIcon cat = new CategoryIcon();
			List <CategoryIcon> listCatIcon = new ArrayList<CategoryIcon>();
			for (Classification classif : classifEligibility) {
				// erreur car on ne trove pas la hierarchie
				// on devrait rentrer la catégorie et trouver l'offre eligible
				// ou alors ne remplir que les parents dans la table st
				// selectionner tous les enfants
				log.info(classif.getLabel().toString());
				if (classif.getLabel().equalsIgnoreCase(categoryRequest)) {
					log.info(classif.getParents().toString());
					listOfEligibleOffersForCategory.add(eligibleOffer);
					cat.setId(classif.getCode());
					cat.setName(classif.getLabel());
					listCatIcon.add(cat);
					
				}

			}
			product.setCategories(listCatIcon);
			res.add(product);

		

			for (EligibleOffer eligibileofferPerCategory : listOfEligibleOffersForCategory) {
				log.info(eligibileofferPerCategory.getOfferSpecification().toString());

				Collection<EligibleOfferAction> eligibleOfferActionForAcquisition = eligibilityService
						.getEligibleActionsForAcquisition(customerOrder,
								new String[] { eligibileofferPerCategory.getOfferSpecification().getCode() });
				for (EligibleOfferAction eligibleOfferActionItem : eligibleOfferActionForAcquisition) {
					log.info(eligibleOfferActionItem.getAction().getActionType().toString());
					log.info("isSingleton" + eligibleOfferActionItem.getAction().getActionType().isSingleton());
					log.info(eligibleOfferActionItem.getAction().getActionType().name().toString());
				}

				Collection<EligibleOfferAction> eligibleOfferActionFinal = eligibilityService
						.getEligibleActions(eliRequest, null);
				for (EligibleOfferAction eligibleOfferActionItem : eligibleOfferActionFinal) {
					log.info(eligibleOfferActionItem.getAction().getActionType().toString());
					log.info("isSingleton" + eligibleOfferActionItem.getAction().getActionType().isSingleton());
					log.info(eligibleOfferActionItem.getAction().getActionType().name().toString());
				}

			}

		}

		log.info("taille 2 = " + listOfEligibleOffersForCategory.size());

//		Page<ProductOfferingIcon> productsEligible = new PageImpl<ProductOfferingIcon>(res);
//		PagedResourcesAssembler<ProductOfferingIcon> pagedResourcesAssembler = new PagedResourcesAssembler<ProductOfferingIcon>(
//				null, null);
//		EligibilityResourceAssembler eligibilityResourceAssembler = new EligibilityResourceAssembler();

	}

	@Test
	public void testGetListEligibleOffersForMigration() {
//		String categoryRequest = "Offres Jordanie";
		Date virtualDate = Calendar.getInstance().getTime();
		Catalogue catalogue;
		CustomerOrder customerOrder = new CustomerOrder();
		catalogue = getCatalogue(virtualDate);

		customerOrder.setCreationDate(virtualDate);
		customerOrder.setConfigurationDate(virtualDate);

		// productInventoryId CONTR0000001466
		customerOrder.setCatalogueVersion(catalogue.getVersion());
		customerOrder.setDistributorChannelId("PHONE");

		List<ProductOfferingIcon> res = new ArrayList<ProductOfferingIcon>();

//		List<EligibleOffer> listOfEligibleOffersForCategory = new ArrayList<EligibleOffer>();
//		CustomerOrderItem customerOrderItem = null;
		// customerOrder.addCustomerOrderItem(customerOrderItem);
//		Collection<CustomerOrderItem> customerOrderItems = customerOrder.getCustomerOrderItems();
//		EligibilityRequest eliRequest;
		ContractAcquisitionEligibilityRequest acqRequest1 = ContractAcquisitionEligibilityRequest.build()
				.withContext(EligibilityRequestContext.build(customerOrder).create()).create();
		EligibilityRequestContract eligibilityRequestContract = EligibilityRequestContract.build()
				.withContractCode("OJPG_CT_STD").create();

//		EligibilityRequestContext eliRequest1;

//		MigrationEligibilityRequest migRequest = null;

	}

	private Catalogue getCatalogue(Date virtualDate) {

		Catalogue cat = new Catalogue();

		cat = catalogueService.getCatalogue(virtualDate);
		return cat;
	}

	@Test
	public void testGetListEligibleOffersForCommercialOperationsForAcquisition() {
		Catalogue catalogue;
		CustomerOrder customerOrder = new CustomerOrder();
		Date virtualDate = Calendar.getInstance().getTime();

		catalogue = getCatalogue(virtualDate);
		if (virtualDate != null) {
			customerOrder.setCreationDate(virtualDate);
			customerOrder.setConfigurationDate(virtualDate);

		} else {
			customerOrder.setConfigurationDate(catalogue.getEffectiveDate());
		}
		customerOrder.setCatalogueVersion(catalogue.getVersion());
		customerOrder.setCatalogueVersion(catalogue.getCastorVersion());
		customerOrder.setDistributorChannelId("WEB");

		Collection<EligibleOfferAction> eligibleActionsForAcquisition = new ArrayList<EligibleOfferAction>();
//		String[] array = new String[] { "BSSAO006" };

		eligibleActionsForAcquisition = eligibilityService.getEligibleActionsForAcquisition(customerOrder, null);

		Collection<EligibleOfferAction> eligibleActionsForMigration = new ArrayList<EligibleOfferAction>();
//		Collection<CustomerOrderItem> customerOrderItems = new ArrayList<CustomerOrderItem>();
//		CustomerOrderItem contractCOI = new CustomerOrderCompositeItem();
//		ContractTypeSpecification contractTypeSpecification = new ContractTypeSpecification();

		CustomerOrder order = new CustomerOrder();
		if (virtualDate != null) {
			order.setCreationDate(virtualDate);
			order.setConfigurationDate(virtualDate);

		} else {
			order.setConfigurationDate(catalogue.getEffectiveDate());
		}
		order.setCatalogueVersion(catalogue.getVersion());
		order.setCatalogueVersion(catalogue.getCastorVersion());
		order.setDistributorChannelId("PHONE");

		CustomerOrderCompositeItem contract = new CustomerOrderCompositeItem();
		contract.setId("001");
		contract.setOfferSpecification(catalogue.getOfferSpecification("GOC11000000006"));

		order.addCustomerOrderItem(contract);

//		List<InstalledOffer> listInstalledOffer = new ArrayList<InstalledOffer>();
//		InstalledOffer installedOffer = new InstalledCompositeOffer();

		eligibleActionsForMigration = eligibilityService.getEligibleActionsForServiceManagement(order, null);
		EligibleOfferAction eli = new EligibleOfferAction();
		for (EligibleOfferAction eof : eligibleActionsForMigration) {
			eli = eof;
			log.info(eli.getAction().toString());
			eli.getOffer();
			eli.getOfferCode();
		}
	}

	@Test
	@Ignore
	public void testGetListEligibleOffersForMigrationFromBss() {
		Catalogue catalogue;
		CustomerOrder customerOrder = new CustomerOrder();
		Date virtualDate = Calendar.getInstance().getTime();

		catalogue = getCatalogue(virtualDate);
		if (virtualDate != null) {
			customerOrder.setCreationDate(virtualDate);
			customerOrder.setConfigurationDate(virtualDate);

		} else {
			customerOrder.setConfigurationDate(catalogue.getEffectiveDate());
		}
		// productInventoryId CONTR0000001466
		customerOrder.setCatalogueVersion(catalogue.getVersion());
		customerOrder.setDistributorChannelId("PHONE");

		Products resp = new Products();
		try {
			RestTemplate restTemplate = new TestRestTemplate();
			String url = urProductInventoryAPI ;
			resp = restTemplate.getForObject(urProductInventoryAPI + "CONTR0000002702",
//					"http://dvdsib94.rouen.francetelecom.fr:8080/apiv3-ws/rest/productInventory/v4/CONTR0000002702",
					
					Products.class);
			// String id = resp.getLinks().getCatalog().getName();
		} catch (Throwable t) {
			t.printStackTrace();
		}
//		String finalChaine;
//		boolean finish = true;
//
		String productOffering = null;
		List<Product> listOfProduct = resp.getProduct();
		for (Product product : listOfProduct) {
			if (product.getId().equalsIgnoreCase("CONTR0000002702")) {
				productOffering = product.getProductOffering().getId();
			}
		}

		Collection<CustomerOrderItem> customerOrderItems = new ArrayList<CustomerOrderItem>();
		CustomerOrderItem contractCOI = new CustomerOrderCompositeItem();

		customerOrderItems.add(contractCOI);
		List<InstalledOffer> listInstalledOffer = new ArrayList<InstalledOffer>();
		InstalledOffer installedOffer = new InstalledCompositeOffer();
		ContractTypeSpecification contractTypeSpecification = new ContractTypeSpecification();

		// installedOffer.setId("GOC11000000006");
		contractTypeSpecification.setCode("GOC11000000003");

		((InstalledCompositeOffer) installedOffer).setCompositeOffer(contractTypeSpecification);

		listInstalledOffer.add(installedOffer);

		customerOrder.addCustomerOrderItem(contractCOI);

		installedOffer.getOfferSpecification();

		MigrationEligibilityRequest migRequest = MigrationEligibilityRequest.build()
				.withContext(EligibilityRequestContext.build(customerOrder).create())
				.withSourceInstalledOffers(listInstalledOffer).create();

		EligibleOffers foundOffers = eligibilityService.getEligibleOffersForMigration(migRequest);
		List<EligibleOffer> listOfEligibleOffers = new ArrayList<EligibleOffer>();
		listOfEligibleOffers = foundOffers.getEligibleOffers();
		Collection<Classification> classifications = new ArrayList<Classification>();
		for (EligibleOffer eligibleOffer : listOfEligibleOffers) {
			ProductOfferingIcon product = new ProductOfferingIcon();
			OfferSpecification offerSpecification = eligibleOffer.getOfferSpecification();

			product.setGenericProduct(offerSpecification.getGenericProduct());

			product.setId(offerSpecification.getCode());

			product.setName(offerSpecification.getLabel());

			classifications = offerSpecification.getClassifications();
		}
	}

	@Test
	public void testGetListEligibleOffersForMigrationFromBssWithProductOffering() {
		Catalogue catalogue;
		CustomerOrder customerOrder = new CustomerOrder();
		Date virtualDate = Calendar.getInstance().getTime();

		catalogue = getCatalogue(virtualDate);
		if (virtualDate != null) {
			customerOrder.setCreationDate(virtualDate);
			customerOrder.setConfigurationDate(virtualDate);

		} else {
			customerOrder.setConfigurationDate(catalogue.getEffectiveDate());
		}
		// productInventoryId CONTR0000001466
		customerOrder.setCatalogueVersion(catalogue.getVersion());
		customerOrder.setDistributorChannelId("WEB");
		ContractTypeSpecification contractTypeSpecification = new ContractTypeSpecification();

		// installedOffer.setId("GOC11000000006");
		contractTypeSpecification.setCode("GOC11000000003");

		CustomerOrderItem contractCOI = new CustomerOrderCompositeItem();

		List<InstalledOffer> listInstalledOffer = new ArrayList<InstalledOffer>();
		InstalledOffer installedOffer = new InstalledCompositeOffer();

		((InstalledCompositeOffer) installedOffer).setCompositeOffer(contractTypeSpecification);

		listInstalledOffer.add(installedOffer);

		customerOrder.addCustomerOrderItem(contractCOI);

		MigrationEligibilityRequest migRequest = MigrationEligibilityRequest.build()
				.withContext(EligibilityRequestContext.build(customerOrder).create())
				.withSourceInstalledOffers(listInstalledOffer).create();

		EligibleOffers foundOffers = eligibilityService.getEligibleOffersForMigration(migRequest);
		List<EligibleOffer> listOfEligibleOffers = new ArrayList<EligibleOffer>();
		listOfEligibleOffers = foundOffers.getEligibleOffers();
		System.out.println("sdhgds");

	}

}
