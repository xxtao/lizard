package com.orange.lizard.eligibility.resource;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collection;
import java.util.Date;
import java.util.List;

import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.IntegrationTest;
//import org.springframework.boot.actuate.health.DataSourceHealthIndicator.Product;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.boot.test.WebIntegrationTest;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.francetelecom.soft.kernel.model.catalogue.Catalogue;
import com.francetelecom.soft.kernel.model.catalogue.ContractTypeSpecification;
import com.francetelecom.soft.kernel.model.configurator.Option;
import com.francetelecom.soft.kernel.model.customerorder.CommercialOperationType;
import com.francetelecom.soft.kernel.model.customerorder.CustomerOrder;
import com.francetelecom.soft.kernel.model.customerorder.CustomerOrderCompositeItem;
import com.francetelecom.soft.kernel.model.customerorder.CustomerOrderItem;
import com.francetelecom.soft.kernel.model.installedbase.InstalledOffer;
import com.francetelecom.soft.kernel.service.catalogue.CatalogueService;
import com.francetelecom.soft.kernel.service.configurator.ConfigurationSession;
import com.francetelecom.soft.kernel.service.configurator.ConfiguratorService;
import com.francetelecom.soft.kernel.service.configurator.command.addcontract.AddContractIconRequest;
import com.francetelecom.soft.kernel.service.configurator.command.initialize.InitializeIconRequest;
import com.francetelecom.soft.kernel.service.eligibility.EligibilityService;
import com.orange.lizard.eligibility.LizardEligibilityApplication;


@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = LizardEligibilityApplication.class)
@IntegrationTest("server.port:0")
@WebIntegrationTest({ "spring.profiles.active=native",
		"spring.cloud.config.server.native.searchLocations=classpath:/operationCommercialeWithoutParcElement.properties","lizard.catalogue.path=classpath:/tmp/catalog" })
public class EligibilityOptionControllerTest {

	private static final Logger log = LoggerFactory.getLogger(EligibilityOptionControllerTest.class);

	@Autowired
	private EligibilityService eligibilityService;

	@Autowired
	private CatalogueService catalogueService;

	@Autowired
	private ConfiguratorService configuratorService;
	
//	@Autowired
//	private Client client;

//	@Test
//	public void testCientFeign() throws BusinessException {
//		Resources<EligibleOption>  listEligibleOptions = client.listEligibleOptions("CONTR0000002702", null, "WEB",null, null);
//		Collection<EligibleOption> listOfEligibleOptions = listEligibleOptions.getContent();
//		Iterator<EligibleOption> it = listOfEligibleOptions.iterator() ;
//		 while (it.hasNext()) {
//			 EligibleOption eligibleOption =it.next() ;
//			 String ident = eligibleOption.getIdent();
//			 String optionType= eligibleOption.getOptionType();
//			 List<Link> link = eligibleOption.getLinks();
//			 System.out.println("test");
//			 
//		 }
//		List<Link> test =listEligibleOptions.getLinks();
//		System.out.println("yezrttyze");
//	}	
	


	@Test
	@Ignore
	public void testCientFeign() {
		// PagedResources<BasicProductOffering> listOfContracts =
		// client.listProductOfferingForCustomer("WEB", null, null, null,
		// "CONTR0000002702", null, null);
		System.out.println("yezrttyze");
	}

	@Test
	public void testInitializeConfigurationWithCustomerOrder() {

		Date virtualDate = Calendar.getInstance().getTime();
		Catalogue catalogue;
		CustomerOrder customerOrder = new CustomerOrder();
		catalogue = getCatalogue(virtualDate);

		customerOrder.setCreationDate(virtualDate);
		customerOrder.setConfigurationDate(virtualDate);

		// productInventoryId CONTR0000001466
		customerOrder.setCatalogueVersion(catalogue.getVersion());
		customerOrder.setDistributorChannelId("PHONE");

		customerOrder.setCreatorProcessId("ACQ");
		customerOrder.setCurrency("EUR");

		ConfigurationSession cfg = configuratorService
				.initialize(InitializeIconRequest.build().withCustomerOrder(customerOrder).create());
		AddContractIconRequest.Builder acqReqBuilder = (AddContractIconRequest.Builder) AddContractIconRequest.build()
				.withConfigurationSession(cfg).withContractCode("BSSCO001");

		String idContract = configuratorService.addContract(acqReqBuilder.create());
		Option contract = configuratorService.getConfiguredOptionByCode(cfg, "BSSCO001");
		configuratorService.select(cfg, configuratorService.getConfiguredOptionByCode(cfg, "BSSAO010").getId());
		Option set = configuratorService.getConfiguredOptionByCode(cfg, "BSSSET004");
		for (int i = 0; i < 3; i++) {
			configuratorService.add(cfg, set.getId(), "BSSAO010");
			set = configuratorService.getConfiguredOptionByCode(cfg, "BSSSET004");
			System.out.println("ici");
		}
		CustomerOrder genOrder = configuratorService.submitConfiguration(cfg);

		cfg = configuratorService.initializeConfiguration(genOrder);

		// Option contract = configuratorService.getConfiguredOptionByCode(cfg,
		// "MDT_C_MeditelConfort");

	}

	private List<InstalledOffer> getSourceInstalledOffers(Collection<CustomerOrderItem> customerOrderItems) {
		List<InstalledOffer> result = new ArrayList<InstalledOffer>();
		for (CustomerOrderItem customerOrderItem : customerOrderItems) {
			if ((customerOrderItem.getCommercialOperationType() != CommercialOperationType.add)
					&& (customerOrderItem instanceof CustomerOrderCompositeItem)
					&& (((CustomerOrderCompositeItem) customerOrderItem)
							.getCompositeOfferSpecification() instanceof ContractTypeSpecification)) {
				for (InstalledOffer installedOfferContract : customerOrderItem.getCustomerOrder().getHolder()
						.getTopInstalledOffers()) {
					if (installedOfferContract.getOfferSpecification().getCode()
							.equals(((CustomerOrderCompositeItem) customerOrderItem).getCompositeOfferSpecification()
									.getCode())) {
						result.add(installedOfferContract);

					}
				}
			}

		}
		return result;
	}

	private Catalogue getCatalogue(Date virtualDate) {

		Catalogue cat = new Catalogue();

		cat = catalogueService.getCatalogue(virtualDate);
		return cat;
	}

}
