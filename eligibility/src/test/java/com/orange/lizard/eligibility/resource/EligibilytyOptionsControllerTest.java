package com.orange.lizard.eligibility.resource;

import java.util.Calendar;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.IntegrationTest;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.boot.test.WebIntegrationTest;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.francetelecom.soft.kernel.exception.BusinessException;
import com.francetelecom.soft.kernel.model.catalogue.AtomicOfferSpecification;
import com.francetelecom.soft.kernel.model.catalogue.Catalogue;
import com.francetelecom.soft.kernel.model.catalogue.ProductType;
import com.francetelecom.soft.kernel.model.configurator.Option;
import com.francetelecom.soft.kernel.model.customerorder.CommercialOperationType;
import com.francetelecom.soft.kernel.model.customerorder.CustomerOrder;
import com.francetelecom.soft.kernel.model.customerorder.CustomerOrderAtomicItem;
import com.francetelecom.soft.kernel.model.customerorder.CustomerOrderCompositeItem;
import com.francetelecom.soft.kernel.model.customerorder.CustomerOrderProductServiceItem;
import com.francetelecom.soft.kernel.model.customerorder.ProductServiceAction;
import com.francetelecom.soft.kernel.model.installedbase.InstalledAtomicOffer;
import com.francetelecom.soft.kernel.model.installedbase.InstalledCompositeOffer;
import com.francetelecom.soft.kernel.model.installedbase.InstalledOffer;
import com.francetelecom.soft.kernel.service.catalogue.CatalogueService;
import com.francetelecom.soft.kernel.service.configurator.ConfigurationSession;
import com.francetelecom.soft.kernel.service.configurator.ConfiguratorService;
import com.francetelecom.soft.kernel.service.configurator.command.addcontract.AddContractIconRequest;
import com.francetelecom.soft.kernel.service.configurator.command.initialize.InitializeIconRequest;
import com.orange.lizard.commons.exception.InvalidQueryException;
import com.orange.lizard.commons.model.installedbase.BssInstalledCompositeOffer;
import com.orange.lizard.commons.service.InstalledBaseService;
import com.orange.lizard.eligibility.LizardEligibilityApplication;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = LizardEligibilityApplication.class)
@IntegrationTest("server.port:0")
@WebIntegrationTest({ "spring.profiles.active=native",
		"spring.cloud.config.server.native.searchLocations=classpath:/operationCommercialeWithoutParcElement.properties","lizard.catalogue.path=classpath:/tmp/catalog" })
public class EligibilytyOptionsControllerTest {


	@Autowired
	private CatalogueService catalogueService;

	@Autowired
	private ConfiguratorService configuratorService;

	@Autowired
	private InstalledBaseService installedBaseService;

	private long id;

	@Test
	@Ignore
	public void testLoadInstalledBase() throws BusinessException {

		BssInstalledCompositeOffer contract = (BssInstalledCompositeOffer) installedBaseService
				.getInstalledOfferById("CONTR0000002702");

		if (contract.getProductSpecification().getProductType() != ProductType.ContractType) {
			throw new InvalidQueryException("Bad request : installed id must match an existing installed contract!");
		}

		// Build CustomerOrder
		CustomerOrder customerOrder = new CustomerOrder();
		customerOrder.setHolder(contract.getHolder());

		Date virtualDate = Calendar.getInstance().getTime();
		String channel = "WEB";
		customerOrder.setDistributorChannelId(channel);
		Catalogue catalogue = getCatalogue();
		customerOrder.setCatalogueVersion(catalogue.getVersion());
		if (virtualDate != null) {
			customerOrder.setCreationDate(virtualDate);
			customerOrder.setConfigurationDate(virtualDate);
		} else {
			customerOrder.setConfigurationDate(catalogue.getEffectiveDate());
		}

		customerOrder.setDoManagePendingInstalledStatus(true);

		ConfigurationSession session = configuratorService.initializeConfiguration(customerOrder);
		String id = configuratorService.loadInstalledOffer(session, contract.getId());
		Collection<Option> options = configuratorService.getConfiguredOptions(session);

		System.out.println("END");

	}

	@Test
	@Ignore
	public void testGetWSFindAndGetInstallPark() throws BusinessException {

		BssInstalledCompositeOffer contract = (BssInstalledCompositeOffer) installedBaseService
				.getInstalledOfferById("CONTR0000002702");

		if (contract.getProductSpecification().getProductType() != ProductType.ContractType) {
			throw new InvalidQueryException("Bad request : installed id must match an existing installed contract!");
		}

		// Build CustomerOrder
		CustomerOrder customerOrder = new CustomerOrder();
		customerOrder.setHolder(contract.getHolder());

		Date virtualDate = Calendar.getInstance().getTime();
		String channel = "WEB";
		customerOrder.setDistributorChannelId(channel);
		Catalogue catalogue = getCatalogue();
		customerOrder.setCatalogueVersion(catalogue.getVersion());
		if (virtualDate != null) {
			customerOrder.setCreationDate(virtualDate);
			customerOrder.setConfigurationDate(virtualDate);
		} else {
			customerOrder.setConfigurationDate(catalogue.getEffectiveDate());
		}

		// contrat
		InstalledCompositeOffer pere = new BssInstalledCompositeOffer();
		// pere
		pere.setId(contract.getId());
		// pere.addStatus(offerStatus);

		pere.setOfferSpecification(contract.getProductSpecification());

		customerOrder.setHolder(contract.getHolder());

		// play set atomic
		List<InstalledOffer> listio = contract.getChildren();
		// CustomerOrderCompositeItem father = cocContract;
		// PLAY
		for (InstalledOffer io : listio) {
			if (io.getProductSpecification().getProductType().equals(ProductType.Play)
					|| io.getProductSpecification().getProductType() != null) {
				// CustomerOrderCompositeItem coci =
				// transformIntalledOfferCPSToCOC((BssInstalledCompositeOffer)
				// io);
				InstalledCompositeOffer ioco = new InstalledCompositeOffer();
				ioco = (InstalledCompositeOffer) io;
				List<InstalledOffer> listOfSetOrAo = ioco.getChildren();
				// father.addChild(coci);
				// customerOrder.addCustomerOrderItem(coci);
				// father = coci;
				// biaf7492
				customerOrder.getHolder().addInstalledOffer(ioco);
				// coci.setParentInstalledCompositeOfferId(contract.getId());

				// tranformSet Or Atomic
				for (InstalledOffer ioSetorAtom : listOfSetOrAo) {
					if (ioSetorAtom.getProductSpecification().getProductType().equals(ProductType.Set)
							|| ioSetorAtom.getProductSpecification().getProductType() != null) {
						// CustomerOrderCompositeItem cociSet =
						// transformIntalledOfferCPSToCOC(
						// (BssInstalledCompositeOffer) ioSetorAtom);
						InstalledCompositeOffer iocoSet = new InstalledCompositeOffer();
						iocoSet = (InstalledCompositeOffer) ioSetorAtom;
						List<InstalledOffer> listOfAto = iocoSet.getChildren();
						// add Set to Play
						// father.addChild(cociSet);
						// coci.addChild(cociSet);
						// customerOrder.addCustomerOrderItem(cociSet);
						// biaf7492
						customerOrder.getHolder().addInstalledOffer(iocoSet);
						// transformAllAtomicOffersToCustomerOrderAtomicItem(listOfAto,
						// customerOrder,father);
						// transformAllAtomicOffersToCustomerOrderAtomicItem(listOfAto,
						// customerOrder,cociSet);
					} else if (ioSetorAtom.getProductSpecification().getProductType().equals(ProductType.AtomicOffer)
							|| ioSetorAtom.getProductSpecification().getProductType() != null) {
						// father
						// CustomerOrderAtomicItem coai =
						// transformAtomicOfferToCustomerOrderAtomicItem(ioSetorAtom);
						// father= Play because no Set
						// father.addChild(coai);
						// customerOrder.addCustomerOrderItem(coai);
						// biaf7492
						customerOrder.getHolder().addInstalledOffer(ioSetorAtom);
					}

				}

			}

		}

		customerOrder.setDoManagePendingInstalledStatus(true);

		ConfigurationSession session = configuratorService.initializeConfiguration(customerOrder);
		InstalledOffer zeContract = configuratorService.getInstalledOffer(session, contract.getId());

		Collection<Option> options = configuratorService.getConfiguredOptions(session);
		System.out.println("ici");

	}

	private CustomerOrderAtomicItem transformAtomicOfferToCustomerOrderAtomicItem(InstalledOffer ioSetorAtom) {
		CustomerOrderAtomicItem contract = new CustomerOrderAtomicItem();
		// BFO
		// contract.setId((ioSetorAtom.getId()));
		contract.setId(getNextId());
		contract.setInstalledOfferId(ioSetorAtom.getId());
		contract.setAtomicOfferSpecification((AtomicOfferSpecification) ioSetorAtom.getProductSpecification());
		contract.setCommercialOperationType(CommercialOperationType.unchange);
		contract.setParentInstalledCompositeOfferId(ioSetorAtom.getParent().getId());

		CustomerOrderProductServiceItem productItem = new CustomerOrderProductServiceItem();
		productItem.setAction(ProductServiceAction.unchange);
		productItem.setId(getNextId());
		productItem.setInstalledProductServiceId(
				((InstalledAtomicOffer) ioSetorAtom).getInstalledProductService().getId());
		contract.setProductServiceOrderItem(productItem);
		// productItem.setParentInstalledAtomicOfferId(contract.getId());
		productItem.setParentInstalledAtomicOfferId(ioSetorAtom.getId());
		Catalogue catalogue = catalogueService.getCatalogue();
		AtomicOfferSpecification aos = (AtomicOfferSpecification) catalogue
				.getOfferSpecification(ioSetorAtom.getOfferSpecification().getCode());
		productItem.setProductServiceSpecification(aos.getProductSpecification());
		return contract;
	}

	private void transformAllAtomicOffersToCustomerOrderAtomicItem(List<InstalledOffer> listOfSet,
			CustomerOrder customerOrder, CustomerOrderCompositeItem father) {
		for (InstalledOffer elementAtom : listOfSet) {
			CustomerOrderAtomicItem coaitransform = transformAtomicOfferToCustomerOrderAtomicItem(elementAtom);

			father.addChild(coaitransform);
			customerOrder.addCustomerOrderItem(coaitransform);
			// biaf7492
			customerOrder.getHolder().addInstalledOffer(elementAtom);
			// coaitransform.setParentInstalledCompositeOfferId(father.getInstalledOfferId());
		}

	}

	private CustomerOrderCompositeItem transformIntalledOfferCPSToCOC(BssInstalledCompositeOffer parent) {

		CustomerOrderCompositeItem contract = new CustomerOrderCompositeItem();
		// BFO
		// contract.setId(parent.getId());
		contract.setInstalledOfferId(parent.getId());
		contract.setId(getNextId());
		contract.setCommercialOperationType(CommercialOperationType.unchange);
		if (parent.getParent() != null) {
			contract.setParentInstalledCompositeOfferId(parent.getParent().getId());
		}
		// ompositeOfferSpecification composite
		contract.setCompositeOfferSpecification(parent.getProductSpecification());

		return contract;

	}

	@Test
	public void testInitializeConfigurationWithCustomerOrder() {
//		final String CONTRACT = "MDT_C_MeditelConfort";
		final String CONTRACT = "BSSCO001";

		Date virtualDate = Calendar.getInstance().getTime();
		Catalogue catalogue;
		CustomerOrder otherCustomerOrder = new CustomerOrder();
		catalogue = getCatalogue(virtualDate);

		otherCustomerOrder.setCreationDate(virtualDate);
		otherCustomerOrder.setConfigurationDate(virtualDate);

		// productInventoryId CONTR0000001466
		otherCustomerOrder.setCatalogueVersion(catalogue.getVersion());
		otherCustomerOrder.setDistributorChannelId("PHONE");

		otherCustomerOrder.setCreatorProcessId("ACQ");
		otherCustomerOrder.setCurrency("EUR");

		ConfigurationSession cfg = configuratorService
				.initialize(InitializeIconRequest.build().withCustomerOrder(otherCustomerOrder).create());
		AddContractIconRequest.Builder acqReqBuilder = (AddContractIconRequest.Builder) AddContractIconRequest.build()
				.withConfigurationSession(cfg).withContractCode(CONTRACT);

		String idContract = configuratorService.addContract(acqReqBuilder.create());

		CustomerOrder genOrder = configuratorService.submitConfiguration(cfg);

		cfg = configuratorService.initializeConfiguration(genOrder);

		Option contract = configuratorService.getConfiguredOptionByCode(cfg, CONTRACT);

	}

	@Test
	public void testParsingEligibleCriterias() {
		Map<String, String> myMap = new HashMap<String, String>();
		// String criterias = "key1%3Dvalue1%26key2%3Dvalue2%26key3%3Dvalue3";
		// String criterias = "key1%3Dvalue1";
		String criterias = "key1%3Dvalue1&key2%3Dvalue2&key3%3Dvalue3";
		String[] tokens = null;
		if (criterias.split("%26") != null) {
			tokens = criterias.split("%26");
		} else {
			tokens[0] = criterias;
		}
		for (String t : tokens) {
			String[] keyValue = t.split("%3D");
			String key = keyValue[0];
			String value = keyValue[1];
			myMap.put(key, value);

		}
	}

	private Catalogue getCatalogue() {

		Catalogue cat = new Catalogue();

		cat = catalogueService.getCatalogue();
		return cat;
	}

	private Catalogue getCatalogue(Date virtualDate) {

		Catalogue cat = new Catalogue();

		cat = catalogueService.getCatalogue(virtualDate);
		return cat;
	}

	private String getNextId() {
		return "0000" + Long.toString(++id);
	}

}
