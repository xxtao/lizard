package com.orange.lizard.eligibility.resource;

import static com.jayway.restassured.RestAssured.expect;
import static com.jayway.restassured.RestAssured.reset;
import static com.jayway.restassured.parsing.Parser.JSON;

import org.junit.After;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.IntegrationTest;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.boot.test.WebIntegrationTest;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.web.context.WebApplicationContext;

import com.jayway.restassured.RestAssured;
import com.orange.lizard.eligibility.LizardEligibilityApplication;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = LizardEligibilityApplication.class)
//@WebAppConfiguration  //dit a Spring de lancer une WebApplicationContext pour les tests
@IntegrationTest("server.port:0") //lancement d'un serveur par Springboot. On peut surcharger toute variable d'env (separes par des ;) 
@WebIntegrationTest({"spring.profiles.active=native",
	"spring.cloud.config.server.native.searchLocations=classpath:/operationCommercialeWithoutParcElement.properties","lizard.catalogue.path=classpath:/tmp/catalog"})                                                                                                                                             // ici, le port par defaut du serveur est surcharge. 0=ouvrir un port disponible.


public class EligibilityApiITTest {
	
    @Autowired
    private WebApplicationContext context;

    @Value("${local.server.port}")  //recuperation du port utilise par la Springboot Application
    private int serverPort;

    @Before
    public void setUp() {
                   RestAssured.baseURI  = "http://localhost";
                   RestAssured.port = serverPort;
                   //RestAssured.basePath = "/v1";
                   RestAssured.defaultParser = JSON;
                   // create dummy tags
                   //when().get("/tags");
    }
    
    @Test
    public void test_UriNotFound() throws Exception {
                   expect().statusCode(404).when().get("/toto");
    }
    
    @Ignore
    @Test
    public void test_GetEligibleContracts_Web_success() throws Exception {
    	
    	expect().statusCode(200).given().parameters("channel","WEB").when().get("/eligibleContracts");
    	
    }
    
    @Ignore
    @Test
    public void test_GetEligibleContracts_Web_CheckValue_success() throws Exception {
    	
    	expect().statusCode(200).given().parameters("channel","WEB").when().get("/eligibleContracts");
    	
    	
    }
    


    @After
    public void tearDown() {
    				reset();
    }
    ///eligibleContracts?channelId=web

}
