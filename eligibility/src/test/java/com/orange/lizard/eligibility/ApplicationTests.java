package com.orange.lizard.eligibility;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.test.IntegrationTest;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.boot.test.WebIntegrationTest;
import org.springframework.context.annotation.ImportResource;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.orange.lizard.eligibility.LizardEligibilityApplication;


@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = LizardEligibilityApplication.class)
@ImportResource("classpath:applicationContext.xml")
@IntegrationTest("server.port:0")
@WebIntegrationTest({"spring.profiles.active=native",
	"spring.cloud.config.server.native.searchLocations=classpath:/operationCommercialeWithoutParcElement.properties","lizard.catalogue.path=classpath:/tmp/catalog"})
public class ApplicationTests {
	private static final Logger log = LoggerFactory.getLogger(ApplicationTests.class);
		
	
	@Test
	public void contextLoads() {
		
	}
	
}
