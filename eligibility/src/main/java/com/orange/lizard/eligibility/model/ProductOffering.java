package com.orange.lizard.eligibility.model;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.orange.lizard.commons.resource.HalResource;

public class ProductOffering extends HalResource {

	//private String id = null;
	private String name= null;
	private String genericProduct = null;
	
	// HAL-like reserved fields
	private List<Category> categories;
	
	@JsonInclude(Include.NON_EMPTY)
	public List<Category> getCategories() {
		return categories;
	}
	public void setCategories(List<Category> categories) {
		this.categories = categories;
	}
	@JsonProperty("name")
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	@JsonProperty("genericProduct")
	public String getGenericProduct() {
		return genericProduct;
	}
	public void setGenericProduct(String genericProduct) {
		this.genericProduct = genericProduct;
	}
	
	 @Override
	  public String toString()  {
	    StringBuilder sb = new StringBuilder();
	    sb.append("class ProductOffering {\n");
	    
	    sb.append("  id: ").append(this.getIdent()).append("\n");
	    sb.append("  name: ").append(name).append("\n");
	    sb.append("  genericProduct: ").append(genericProduct).append("\n");
	     sb.append("}\n");
	    return sb.toString();
	  }
	
	
}
