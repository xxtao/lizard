package com.orange.lizard.eligibility.resource;

import static org.springframework.hateoas.mvc.ControllerLinkBuilder.linkTo;
import static org.springframework.hateoas.mvc.ControllerLinkBuilder.methodOn;
import static org.springframework.http.MediaType.APPLICATION_JSON_VALUE;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.data.web.PagedResourcesAssembler;
import org.springframework.hateoas.MediaTypes;
import org.springframework.hateoas.PagedResources;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.francetelecom.soft.kernel.exception.BusinessException;
import com.francetelecom.soft.kernel.model.catalogue.Catalogue;
import com.francetelecom.soft.kernel.model.configurator.Option;
import com.francetelecom.soft.kernel.model.customerorder.CustomerOrder;
import com.francetelecom.soft.kernel.model.installedbase.InstalledOffer;
import com.francetelecom.soft.kernel.service.catalogue.CatalogueService;
import com.francetelecom.soft.kernel.service.configurator.ConfigurationSession;
import com.francetelecom.soft.kernel.service.configurator.ConfiguratorService;
import com.francetelecom.soft.kernel.service.configurator.helper.ConfiguratorPropertyEvaluator;
import com.orange.lizard.commons.exception.InvalidQueryException;
import com.orange.lizard.commons.exception.NotFoundException;
import com.orange.lizard.commons.service.InstalledBaseService;
import com.orange.lizard.eligibility.model.CommercialOperations;
import com.orange.lizard.eligibility.model.CommercialOperationsHal;
import com.orange.lizard.eligibility.model.EligibleOption;
import com.orange.lizard.eligibility.model.OperationCommercialeAuthorized;
import com.orange.lizard.eligibility.model.ProductOfferingIcon;

@RestController
@CrossOrigin
@RequestMapping(value = "/commercialOperations", produces = { APPLICATION_JSON_VALUE, MediaTypes.HAL_JSON_VALUE })
public class EligibilityActionController {

	@Autowired
	private com.orange.lizard.eligibility.util.EligibleOptionFactory eligibleOptionFactory;

	@Autowired
	private CatalogueService catalogueService;

	@Autowired
	private ConfiguratorPropertyEvaluator propertyEvaluator;

	@Autowired
	ConfiguratorService configuratorService;

	@Autowired
	private InstalledBaseService installedBaseService;

	@Autowired
	private OperationsCommerciales operationsCommerciales;

	@Autowired
	private com.orange.lizard.eligibility.util.ServiceForHystrix service;

	@Autowired
	private com.orange.lizard.eligibility.util.ServiceDetectXSS serviceDetectXSS;

	private CustomerOrder customerOrder;

	protected Catalogue getCatalogue() {
		// Warn, should use a date from the order for this!
		return catalogueService.getCatalogue();
	}

	private CommercialOperations commercialOperations;

	private Map<String, OperationCommerciale> tableOperations;

	@RequestMapping(method = RequestMethod.GET)
	@ResponseBody
	public PagedResources<CommercialOperationsHal> listCommercialOperations(
			@RequestParam(value = "channel", required = true) String channel,
			@RequestParam(value = "salesPoint", required = false) String salesPoint,
			@RequestParam(value = "partyId", required = false) String partyId,
			@RequestParam(value = "productId", required = false) String productId,
			@RequestParam(value = "eligibleCriterias", required = false) String eligibleCriterias, Pageable pageable)
					throws BusinessException {

		String channelVer = null;
		String salesPointVer = null;
		String partyIdVer = null;
		String productIDVer = null;
		String eligibleCriteriasVer = null;
		
		tableOperations = new HashMap<String, OperationCommerciale>();
		tableOperations = operationsCommerciales.getOperations();


		Set<String> channelTable =new HashSet<String>();
		List<String> channelList = new ArrayList<String>();
		
		for (Entry<String, OperationCommerciale> entry : tableOperations.entrySet()) {

			channelList = entry.getValue().getDetail();
			for (String valueChannel : channelList ) {
				channelTable.add(valueChannel);
			}
		}
		
		boolean findChannel = false;
		if (channel!=null && !channel.equalsIgnoreCase("")) {
			
			channelVer = serviceDetectXSS.detectSpecialCharacter(channel);
			channelVer=channelVer.toUpperCase();
			
			Iterator i=channelTable.iterator();
			
			while(i.hasNext() && !findChannel) 
			{
				
				if(channelVer.equalsIgnoreCase(i.next().toString())){
					findChannel = true;
				}
			}
		}
		
		if (!findChannel || channel==null) {
			throw new InvalidQueryException("Invalid channel parameter");
		}

		if (salesPoint != null && !salesPoint.equalsIgnoreCase("")) {

			salesPointVer = serviceDetectXSS.detectSpecialCharacter(salesPoint);
		}
		if (partyId != null && !partyId.equalsIgnoreCase("")) {

			partyIdVer = serviceDetectXSS.detectOnlyCharacterAndNumber(partyId);
		}
		if (productId != null && !productId.equalsIgnoreCase("")) {

			productIDVer = serviceDetectXSS.detectOnlyCharacterAndNumber(productId);
		}
		if (eligibleCriterias != null && !eligibleCriterias.equalsIgnoreCase("")) {

			eligibleCriteriasVer = serviceDetectXSS.detectInvalidCharacterForCriteria(eligibleCriterias);
		}

		
		List<String> listTemporaireActionCommerciale = new ArrayList<String>();

		List<String> listFinalCommercialsActionsWithContext = new ArrayList<String>();
		List<String>listInitialCommercialsActionsWithContext = new ArrayList<String>();

		List<OperationCommercialeAuthorized> finalListOperationCommercialeAuthorized = new ArrayList<OperationCommercialeAuthorized>();

		List<CommercialOperations> listCommercialOperations = new ArrayList<CommercialOperations>();
		customerOrder = new CustomerOrder();
		Date virtualDate = Calendar.getInstance().getTime();

		Catalogue catalogue = getCatalogue();
		if (virtualDate != null) {
			customerOrder.setCreationDate(virtualDate);
			customerOrder.setConfigurationDate(virtualDate);

		} else {
			customerOrder.setConfigurationDate(catalogue.getEffectiveDate());
		}
		customerOrder.setCatalogueVersion(catalogue.getVersion());

		customerOrder.setDistributorChannelId(channelVer);
		customerOrder.setCatalogueVersion(catalogue.getVersion());
		
		for (Entry<String, OperationCommerciale> entry : tableOperations.entrySet()) {
			if (entry.getValue().getDetail().contains(channelVer)) {
				listInitialCommercialsActionsWithContext.add(entry.getKey());

			}
		}

		if (productIDVer == null || productIDVer.equalsIgnoreCase("")) {

			listFinalCommercialsActionsWithContext = listInitialCommercialsActionsWithContext;
		} else {

			// c'et la ou on effectue la recherche sur productID
			InstalledOffer contract = null;
			try {
				contract = installedBaseService.getInstalledOfferById(productIDVer);
			} catch (BusinessException e) {
				
				throw new NotFoundException("Not Found");
			}
			customerOrder.setHolder(contract.getHolder());
			customerOrder.setDoManagePendingInstalledStatus(true);

			if (eligibleCriteriasVer != null && !eligibleCriteriasVer.equalsIgnoreCase("")) {
				Map<String, String> criterias = eligibleOptionFactory.decodeCriterias(eligibleCriteriasVer);

				for (String key : criterias.keySet()) {
					String value = criterias.get(key);
					customerOrder.setConfigurationContextValue(key, value);
				}
			}

			ConfigurationSession cfg = configuratorService.initializeConfiguration(customerOrder);
			configuratorService.loadInstalledOffer(cfg, productIDVer);
			Option root = configuratorService.getConfiguredOptionById(cfg, productIDVer);
			Collection<String> collectionValues = propertyEvaluator.evaluateProductProperties(cfg,
					productIDVer, "CommercialOperation");


			if (collectionValues == null) {
				// avoid null pointerException
				collectionValues = new ArrayList<String>();
			}
			// TER
			List<EligibleOption> eligibleOptions = eligibleOptionFactory.buildEligibleOptions(root,null);

			if (eligibleOptions != null) {
				EligibleOption eligibleOptionContract = eligibleOptions.get(0);
				if (eligibleOptionContract.isCanBeTerminated()&& (operationsCommerciales.getOperations().get("TER").getDetail().contains(channelVer))) {

					listTemporaireActionCommerciale.add("TER");
				}
				;

				// MOD
				for (EligibleOption eligibleOption : eligibleOptions) {
					if (eligibleOption.isCanBeModified() || eligibleOption.isCanBeSubscribed()) {
						listTemporaireActionCommerciale.add("MOD");
						break;
					}
					;

				}
				// MIG depends on channel
				List<ProductOfferingIcon> res = service.getAllContractsWithoutPage(channelVer, null, null,
						productIDVer, null, eligibleCriteriasVer);
				if ((res.size() != 0) && (operationsCommerciales.getOperations().get("MIG").getDetail().contains(channelVer))) {
					listTemporaireActionCommerciale.add("MIG");
				}
			}

			// other commercials actions
			List<String> otherCommercialsActionsWithContext = new ArrayList<String>();

			for (String collectionValue : collectionValues) {
				List<String> otherCommercialsActions = operationsCommerciales.getOperations().get(collectionValue)
						.getDetail();
				if (otherCommercialsActions.contains(channelVer)) {
					otherCommercialsActionsWithContext.add(collectionValue);
				}

			}

			listFinalCommercialsActionsWithContext.addAll(listTemporaireActionCommerciale);
			listFinalCommercialsActionsWithContext.addAll(otherCommercialsActionsWithContext);

		}

		
		for (String keyInitial : listFinalCommercialsActionsWithContext) {
			OperationCommercialeAuthorized oca = new OperationCommercialeAuthorized();
			oca.setId(keyInitial);
			oca.setName(operationsCommerciales.getOperations().get(keyInitial).getLibelle());
			oca.setIsAuthorized(true);
			finalListOperationCommercialeAuthorized.add(oca);
		}
		
		
		List<OperationCommercialeAuthorized> finalTempoList = new ArrayList<OperationCommercialeAuthorized>(); 		
		for (String keyInitial : listInitialCommercialsActionsWithContext) { 			
			OperationCommercialeAuthorized oca = new OperationCommercialeAuthorized(); 			
			oca.setId(keyInitial); 			
			oca.setName(operationsCommerciales.getOperations().get(keyInitial).getLibelle()); 			
			oca.setIsAuthorized(false); 			
			finalTempoList.add(oca); 		
		}
		
		List<OperationCommercialeAuthorized> finalList = new ArrayList<OperationCommercialeAuthorized>(finalTempoList); 
		
		
		for(int i = 0; i < finalListOperationCommercialeAuthorized.size(); i++) { 			
			for(int j = 0; j < finalTempoList.size(); j++) { 
//				System.out.println("i="+ i + finalListOperationCommercialeAuthorized.get(i).getId());
//				System.out.println("j="+ j + finalTempoList.get(j).getId());
				if (finalListOperationCommercialeAuthorized.get(i).getId().equalsIgnoreCase(finalTempoList.get(j).getId())) { 					
					finalList.remove(finalTempoList.get(j)); 				
					} 			
				} 					
			
		}

		Set<OperationCommercialeAuthorized> set = new HashSet<OperationCommercialeAuthorized>() ; 		
		set.addAll(finalListOperationCommercialeAuthorized) ; 		
		set.addAll(finalList);
		
				
		
		List<OperationCommercialeAuthorized> finalListAuthorized = new ArrayList<OperationCommercialeAuthorized>(set);
		
		
		
		commercialOperations = new CommercialOperations(); 		
		commercialOperations.setCommercialOperations(finalListAuthorized); 		
		if (productIDVer != null && productIDVer != "") { 			
			commercialOperations.setProductInvotenryId(productIDVer); 		
			} 		
		
		listCommercialOperations.add(commercialOperations);
		
		// BIAF7492 : add pageable and total inpiut parameters
		Page<CommercialOperations> actionsEligible = new PageImpl<CommercialOperations>(listCommercialOperations,pageable, listCommercialOperations.size());
		PagedResourcesAssembler<CommercialOperations> pagedResourcesAssembler = new PagedResourcesAssembler<CommercialOperations>(
				null, null);
		CommercialOperationsResourceAssembler commercialOperationsResourceAssembler = new CommercialOperationsResourceAssembler();
		PagedResources<CommercialOperationsHal> resultat = pagedResourcesAssembler
				.toResource(actionsEligible, commercialOperationsResourceAssembler,
						linkTo(methodOn(EligibilityActionController.class).listCommercialOperations(channelVer,
								salesPointVer, partyIdVer, productIDVer, eligibleCriteriasVer, pageable))
										.withSelfRel());

		return resultat;

	}

}
