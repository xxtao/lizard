package com.orange.lizard.eligibility.resource;

import static org.springframework.http.MediaType.APPLICATION_JSON_VALUE;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

import org.owasp.encoder.Encode;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.data.domain.Page;
import org.springframework.data.web.PagedResourcesAssembler;
import org.springframework.hateoas.MediaTypes;
import org.springframework.hateoas.PagedResources;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.orange.lizard.commons.exception.InvalidQueryException;
import com.orange.lizard.eligibility.model.ProductOffering;
import com.orange.lizard.eligibility.model.ProductOfferingIcon;


@EnableConfigurationProperties
@RestController
@CrossOrigin
//@EnableCircuitBreaker
@RequestMapping(value = "/eligibleContracts", produces = { APPLICATION_JSON_VALUE , MediaTypes.HAL_JSON_VALUE })

public class EligibiltyController  {
	@Autowired
	private com.orange.lizard.eligibility.util.ServiceDetectXSS serviceDetectXSS;
	
	@Autowired 	
	private EligibilityResourceAssembler eligibilityResourceAssembler;
	
	@Autowired
	private com.orange.lizard.eligibility.util.ServiceForHystrix service;
	
	@Autowired
	private OperationsCommerciales operationsCommerciales;

	private Map<String, OperationCommerciale> tableOperations;
	

	@RequestMapping(method = RequestMethod.GET)
	@ResponseBody
	public PagedResources<ProductOffering> listProductOfferingForCustomer  (
			@RequestParam(value = "channel", required = true) String channel,
			@RequestParam(value = "salesPoint", required = false) String salesPoint,
			@RequestParam(value = "categoryId", required = false) String categoryId,
			@RequestParam(value = "partyId", required = false, defaultValue = "") String partyId,
			@RequestParam(value = "productId", required = false, defaultValue = "") String productId,
			@RequestParam(value = "productOfferingId", required = false, defaultValue = "") String productOfferingId,
			@RequestParam(value = "eligibleCriterias", required = false) String eligibleCriterias ) {

		String channelVer=null;
		String salesPointVer=null;
		String categoryIDVer=null;
		String partyIdVer=null;
		String productIDVer=null;
		String productOfferingIdVer=null;
		String eligibleCriteriasVer=null;
		
		tableOperations = new HashMap<String, OperationCommerciale>();
		tableOperations = operationsCommerciales.getOperations();
		Set<String> channelTable =new HashSet<String>();
		List<String> channelList = new ArrayList<String>();
		
		for (Entry<String, OperationCommerciale> entry : tableOperations.entrySet()) {

			channelList = entry.getValue().getDetail();
			for (String valueChannel : channelList ) {
				channelTable.add(valueChannel);
			}
		}
		
		boolean findChannel = false;
		if (channel!=null && !channel.equalsIgnoreCase("")) {
			
			channelVer = serviceDetectXSS.detectSpecialCharacter(channel);
			channelVer=channelVer.toUpperCase();
			
			Iterator i=channelTable.iterator();
			
			while(i.hasNext() && !findChannel) 
			{
				
				if(channelVer.equalsIgnoreCase(i.next().toString())){
					findChannel = true;
				}
			}
		}
		
		if (!findChannel || channel==null) {
			throw new InvalidQueryException("Invalid channel parameter");
		}
		if (salesPoint!=null && !salesPoint.equalsIgnoreCase("")) {
			
			salesPointVer = serviceDetectXSS.detectSpecialCharacter(salesPoint);
		}
		if (categoryId!=null && !categoryId.equalsIgnoreCase("")) {
			
			categoryIDVer = serviceDetectXSS.detectInvalidCharacterForCAtegory(categoryId);
		}
		if (partyId!=null && !partyId.equalsIgnoreCase("")) {
			
			partyIdVer = serviceDetectXSS.detectOnlyCharacterAndNumber(partyId);
		}
		if (productId!=null && !productId.equalsIgnoreCase("")) {
			
			productIDVer = serviceDetectXSS.detectOnlyCharacterAndNumber(productId);
		}
		if (productOfferingId!=null && !productOfferingId.equalsIgnoreCase("")) {
			
			productOfferingIdVer = serviceDetectXSS.detectOnlyCharacterAndNumber(productOfferingId);
		}
		if (eligibleCriterias!=null && !eligibleCriterias.equalsIgnoreCase("")) {
			
			eligibleCriteriasVer = serviceDetectXSS.detectInvalidCharacterForCriteria(eligibleCriterias);
		}
		
		
		
		Page<ProductOfferingIcon> productsEligible = service.getAllContracts(channelVer,salesPointVer,categoryIDVer,partyIdVer,productIDVer,productOfferingIdVer,eligibleCriteriasVer);
		Encode.forHtmlAttribute(channelVer);
 		Encode.forHtmlAttribute(salesPointVer);
 		Encode.forHtmlAttribute(partyIdVer);
 		Encode.forHtmlAttribute(productIDVer);
 		Encode.forHtmlAttribute(productOfferingIdVer);
 		Encode.forHtmlAttribute(eligibleCriteriasVer);
 		// BIAF7492 : 21/06/2017
 		// No Pageable is being given to this method ... this makes using the PagedResourceAsembler break down
 		PagedResourcesAssembler<ProductOfferingIcon> pagedResourcesAssembler = new PagedResourcesAssembler<ProductOfferingIcon>(
 				null, null);
 	
 		PagedResources<ProductOffering> result = pagedResourcesAssembler.toResource(productsEligible,
 				eligibilityResourceAssembler);
// 		List<ProductOffering> result = new ArrayList<>();
// 		for (ProductOfferingIcon iconProduct : productsEligible.getContent()) {
// 			result.add(eligibilityResourceAssembler.toResource(iconProduct));
// 		}
 	
 		return result;
	}
	

	} 

