package com.orange.lizard.eligibility.model;

public class OperationCommercialeAuthorized {

	private String name;
	private String id;
	private boolean isAuthorized = false;
	
	public boolean getIsAuthorized() {
		return isAuthorized;
	}
	public void setIsAuthorized(boolean isAuthorized) {
		this.isAuthorized = isAuthorized;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	
}
