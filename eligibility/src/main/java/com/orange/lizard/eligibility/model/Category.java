package com.orange.lizard.eligibility.model;

import com.orange.lizard.commons.resource.HalResource;

public class Category extends HalResource {


	//private String id= null;
	private String name = null;
	
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	
	 @Override
	  public String toString()  {
	    StringBuilder sb = new StringBuilder();
	    sb.append("class Category For o product offering {\n");
	    
	    sb.append("  id: ").append(this.getIdent()).append("\n");
	    sb.append("  name: ").append(name).append("\n");
	    sb.append("}\n");
	    return sb.toString();
	  }

	
}
