package com.orange.lizard.eligibility.resource;

import org.springframework.hateoas.mvc.ResourceAssemblerSupport;

import com.orange.lizard.eligibility.model.CommercialOperations;
import com.orange.lizard.eligibility.model.CommercialOperationsHal;

public class CommercialOperationsResourceAssembler extends ResourceAssemblerSupport<CommercialOperations, CommercialOperationsHal> {

	public CommercialOperationsResourceAssembler() {
		this(EligibilityActionController.class, CommercialOperationsHal.class);
	}
	

	public CommercialOperationsResourceAssembler(Class<?> controllerClass,
			Class<CommercialOperationsHal> resourceType) {
		super(controllerClass, resourceType);
	}

	@Override
	public CommercialOperationsHal toResource(CommercialOperations entity) {
		CommercialOperationsHal res = new CommercialOperationsHal();
		res.setOperationDate(entity.getOperationDate());
		//res.setIdent(id);
		
		res.setCommercialOperations(entity.getCommercialOperations());
		res.setProductInventoryId(entity.getProductInvotenryId());
		return res;
	}
	
	

}
