package com.orange.lizard.eligibility.model;

import java.io.Serializable;
import java.util.List;

public class ProductOfferingIcon implements Serializable {

	private static final long serialVersionUID = 1L;
	private String id ;
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getGenericProduct() {
		return genericProduct;
	}
	public void setGenericProduct(String genericProduct) {
		this.genericProduct = genericProduct;
	}
	private String name;
	private String genericProduct;
	
	private List<CategoryIcon> categories;
	public List<CategoryIcon> getCategories() {
		return categories;
	}
	public void setCategories(List<CategoryIcon> categories) {
		this.categories = categories;
	}
}
