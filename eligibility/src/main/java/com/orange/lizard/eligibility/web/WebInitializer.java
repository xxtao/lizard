package com.orange.lizard.eligibility.web;

import java.util.Enumeration;
import java.util.Properties;

import javax.servlet.ServletContext;
import javax.servlet.ServletException;

import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.boot.context.web.SpringBootServletInitializer;

import com.orange.lizard.eligibility.LizardEligibilityApplication;

/**
 * Package SpringBoot application as WebApplication.
 * 
 * 
 * @author OrangeApplicationsForBusiness
 *
 */
public class WebInitializer extends SpringBootServletInitializer {

	Properties defaultProperties = new Properties();

	/**
	 * SpringBoot doesn't seems to read ContextParameters
	 * 
	 * Spring.cloud don't find spring.cloud.config.server.native.searchLocations
	 * in default properties.
	 * 
	 * if ContextParameter start with system. set this value in
	 * System.properties otherwise, keep contextParameter to configure
	 * spring-boot application in configure(..) method.
	 * 
	 * @param servletContext
	 *            the application context.
	 * 
	 * @throws ServletException
	 */
	@Override
	public void onStartup(ServletContext servletContext) throws ServletException {
		Enumeration<String> e = servletContext.getInitParameterNames();
		while (e.hasMoreElements()) {
			String attr = e.nextElement();
			String value = servletContext.getInitParameter(attr);
			if (null != value) {
				if (attr.startsWith("system.")) {
					attr = attr.substring(7);
					System.setProperty(attr, value);
				} else {
					defaultProperties.put(attr, value);
				}
			}
		}

		// Don't forget to call super method.
		super.onStartup(servletContext);
	}

	/**
	 * Configure logback and default location of application.properties because
	 * we don't want to use spring-profile facilities
	 */
	@Override
	protected SpringApplicationBuilder configure(SpringApplicationBuilder application) {
		String externalconfig = System.getProperty("lizard-api-eligibility.configDirectory");
		if(null==externalconfig){
			externalconfig = System.getProperty("LIZARD_API_ELIGIBILITY_CONFIGDIRECTORY");
		}
		if(null==externalconfig){
			externalconfig = System.getProperty("ELIGIBILITY_CONFDIR");
		}
		if (null != externalconfig) {
			if (!externalconfig.endsWith("/") && !externalconfig.endsWith("\\")) {
				externalconfig = externalconfig + "/";
			}
			defaultProperties.put("spring.config.location", externalconfig);
			defaultProperties.put("logging.config", externalconfig + "logback.xml");
		}

		return application.properties(defaultProperties).sources(LizardEligibilityApplication.class);
	}
}
