package com.orange.lizard.eligibility.resource;

import static org.springframework.http.MediaType.APPLICATION_JSON_VALUE;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.Map.Entry;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.hateoas.MediaTypes;
import org.springframework.hateoas.Resources;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.francetelecom.onecrm.configurator.core.configuration.CoreConfigurationManager;
import com.francetelecom.onecrm.configurator.core.configuration.sessionview.ContractSessionView;
import com.francetelecom.soft.kernel.exception.BusinessException;
import com.francetelecom.soft.kernel.model.catalogue.Catalogue;
import com.francetelecom.soft.kernel.model.catalogue.ProductType;
import com.francetelecom.soft.kernel.model.configurator.Option;
import com.francetelecom.soft.kernel.model.customerorder.CustomerOrder;
import com.francetelecom.soft.kernel.model.installedbase.InstalledOffer;
import com.francetelecom.soft.kernel.service.catalogue.CatalogueService;
import com.francetelecom.soft.kernel.service.configurator.ConfigurationSession;
import com.francetelecom.soft.kernel.service.configurator.ConfiguratorService;
import com.orange.lizard.commons.exception.InvalidQueryException;
import com.orange.lizard.commons.service.InstalledBaseService;
import com.orange.lizard.eligibility.model.EligibleOption;

@RestController
@CrossOrigin
@RequestMapping(value = "/eligibleOptions", produces = { APPLICATION_JSON_VALUE, MediaTypes.HAL_JSON_VALUE })

public class EligibilityOptionController {

	@Autowired
	private CatalogueService catalogueService;
	@Autowired
	ConfiguratorService configuratorService;

	@Autowired
	private InstalledBaseService installedBaseService;

	@Autowired
	private com.orange.lizard.eligibility.util.ServiceDetectXSS serviceDetectXSS;

	@Autowired
	private com.orange.lizard.eligibility.util.EligibleOptionFactory eligibleOptionFactory;
	
	@Autowired
	private OperationsCommerciales operationsCommerciales;

	private Map<String, OperationCommerciale> tableOperations;

	@RequestMapping(method = RequestMethod.GET)
	public ResponseEntity<List<EligibleOption>> listEligibleOptions(
			@RequestParam(value = "contractProductId", required = true) String installedContractId,
			@RequestParam(value = "productOfferingId", required = false) String productOfferingCode,
			@RequestParam(value = "channel", required = true) String channelId,
			@RequestParam(value = "productId", required = false) String installedProductId,
			@RequestParam(value = "eligibleCriterias", required = false) String eligibleCriterias)
					throws BusinessException {

		String installedContractIdVer = null;
		String productOfferingCodeVer = null;
		String channelIdVer = null;
		String installedProductIdVer = null;
		String eligibleCriteriasVer = null;

		if (StringUtils.isEmpty(installedContractId)) {
			throw new InvalidQueryException("Missing information : installed contract id !");
		}
		tableOperations = new HashMap<String, OperationCommerciale>();
		tableOperations = operationsCommerciales.getOperations();

		Set<String> channelTable =new HashSet<String>();
		List<String> channelList = new ArrayList<String>();
		
		for (Entry<String, OperationCommerciale> entry : tableOperations.entrySet()) {

			channelList = entry.getValue().getDetail();
			for (String valueChannel : channelList ) {
				channelTable.add(valueChannel);
			}
		}
		
		boolean findChannel = false;
		if (channelId!=null && !channelId.equalsIgnoreCase("")) {
			
			channelIdVer = serviceDetectXSS.detectSpecialCharacter(channelId);
			channelIdVer=channelIdVer.toUpperCase();
			
			Iterator i=channelTable.iterator();
			
			while(i.hasNext() && !findChannel) 
			{
				
				if(channelIdVer.equalsIgnoreCase(i.next().toString())){
					findChannel = true;
				}
			}
		}
		
		if (!findChannel || channelIdVer==null) {
			throw new InvalidQueryException("Invalid channel parameter");
		}

		if (installedContractId != null && !installedContractId.equalsIgnoreCase("")) {

			installedContractIdVer = serviceDetectXSS.detectOnlyCharacterAndNumber(installedContractId);
		}
		if (productOfferingCode != null && !productOfferingCode.equalsIgnoreCase("")) {

			productOfferingCodeVer = serviceDetectXSS.detectOnlyCharacterAndNumber(productOfferingCode);
		}
		if (installedProductId != null && !installedProductId.equalsIgnoreCase("")) {

			installedProductIdVer = serviceDetectXSS.detectOnlyCharacterAndNumber(installedProductId);
		}

		if (eligibleCriterias != null && !eligibleCriterias.equalsIgnoreCase("")) {

			eligibleCriteriasVer = serviceDetectXSS.detectInvalidCharacterForCriteria(eligibleCriterias);
		}
		// Fetch Installed Contract
		InstalledOffer contract = installedBaseService.getInstalledOfferById(installedContractIdVer);
		if (contract.getProductSpecification().getProductType() != ProductType.ContractType) {
			throw new InvalidQueryException("Bad request : installed id must match an existing installed contract!");
		}

		// Build CustomerOrder
		CustomerOrder customerOrder = new CustomerOrder();
		customerOrder.setHolder(contract.getHolder());

		Date virtualDate = Calendar.getInstance().getTime();
		// String channel = headers.getOrDefault("XXX-X-CHANNEL",
		// Arrays.asList("WEB")).get(0);
		customerOrder.setDistributorChannelId(channelIdVer);
		Catalogue catalogue = getCatalogue();
		customerOrder.setCatalogueVersion(catalogue.getVersion());
		if (virtualDate != null) {
			customerOrder.setCreationDate(virtualDate);
			customerOrder.setConfigurationDate(virtualDate);
		} else {
			customerOrder.setConfigurationDate(catalogue.getEffectiveDate());
		}
		customerOrder.setDoManagePendingInstalledStatus(true);
		if (eligibleCriteriasVer != null && !eligibleCriteriasVer.equalsIgnoreCase("")) {
			Map<String, String> criterias = decodeCriterias(eligibleCriteriasVer);

			for (String key : criterias.keySet()) {
				String value = criterias.get(key);
				customerOrder.setConfigurationContextValue(key, value);
			}
		}

		ConfigurationSession cfg = configuratorService.initializeConfiguration(customerOrder);
		configuratorService.loadInstalledOffer(cfg, installedContractIdVer);
		// getConfiguredOptions according to input params - 3 cases
		Option root = null;
		if (!StringUtils.isEmpty(productOfferingCodeVer)) {
			root = configuratorService.getConfiguredOptionByCode(cfg, productOfferingCodeVer);
		} else if (!StringUtils.isEmpty(installedProductIdVer)) {
			root = configuratorService.getConfiguredOptionById(cfg, installedProductIdVer);
		} else {
			root = configuratorService.getConfiguredOptionById(cfg, installedContractIdVer);
		}
		
		CoreConfigurationManager ccm = cfg.getConfigurationManager();
		ccm.getProductItem(installedContractId);
		//(ConfigurationManager manager, String code, String id)
		ccm.getContractSessionView(ccm.getProductItem(installedContractId));
		//ProductItem item = new ProductItem();
		ContractSessionView contractSessionView = cfg.getConfigurationManager().getContractSessionView(ccm.getProductItem(installedContractId));

		List<EligibleOption> eligibleOptions = eligibleOptionFactory.buildEligibleOptions(root,contractSessionView);
		return new ResponseEntity<List<EligibleOption>>(eligibleOptions, HttpStatus.OK);
//		return new Resources<EligibleOption>(eligibleOptions);

	}

	private Map<String, String> decodeCriterias(String eligibleCriterias) {
		Map<String, String> criterias = new HashMap<String, String>();
		String[] keyValues = eligibleCriterias.split("&");
		for (String eachKeyValue : keyValues) {
			String[] keyValue = eachKeyValue.split("=");
			String key = keyValue[0];
			String value = keyValue[1];
			criterias.put(key, value);

		}
		return criterias;
	}

	private Catalogue getCatalogue() {

		Catalogue cat = new Catalogue();

		cat = catalogueService.getCatalogue();
		return cat;
	}

}
