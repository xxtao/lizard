package com.orange.lizard.eligibility.model;

import java.util.ArrayList;
import java.util.List;

public class ModifyableFunction {
	
	private String id;
	private String name;
	private String value;
	private String systemValueCode;
	
	private List<FunctionValue> productSpecCharacteristicValues = new ArrayList<FunctionValue>();

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setFunctionLabel(String name) {
		this.name = name;
	}

	public String getValue() {
		return value;
	}

	public void setValue(String value) {
		this.value = value;
	}

	public String getSystemValueCode() {
		return systemValueCode;
	}

	public void setSystemValueCode(String systemValueCode) {
		this.systemValueCode = systemValueCode;
	}

	public List<FunctionValue> getProductSpecCharacteristicValues() {
		return productSpecCharacteristicValues;
	}

	public void setProductSpecCharacteristicValues(List<FunctionValue> productSpecCharacteristicValues) {
		this.productSpecCharacteristicValues = productSpecCharacteristicValues;
	}
	
	public void addProductSpecCharacteristicValue(FunctionValue value) {
		this.productSpecCharacteristicValues.add(value);
	}
	
	

}
