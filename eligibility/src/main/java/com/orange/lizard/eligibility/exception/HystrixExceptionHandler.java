package com.orange.lizard.eligibility.exception;

import org.springframework.hateoas.MediaTypes;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

import com.netflix.hystrix.exception.HystrixBadRequestException;
import com.netflix.hystrix.exception.HystrixRuntimeException;
import com.netflix.hystrix.exception.HystrixRuntimeException.FailureType;
import com.orange.lizard.commons.exception.InvalidQueryException;
import com.orange.lizard.commons.model.Error;
import com.orange.lizard.commons.model.ErrorCode;


@ControllerAdvice 
public class HystrixExceptionHandler  extends ResponseEntityExceptionHandler {

	public HystrixExceptionHandler() {
		
	}
	 @ExceptionHandler(value = { HystrixRuntimeException.class }) 
	 public final ResponseEntity<?> handleException(HystrixRuntimeException e, WebRequest request) { 
	  HttpStatus status = HttpStatus.SERVICE_UNAVAILABLE; 
	  String message = e.getMessage(); 

	 
	  FailureType type = e.getFailureType(); 
	  String labelCode = null;
	  	 
	  if (type.equals(FailureType.COMMAND_EXCEPTION)||(type.equals(FailureType.TIMEOUT))) { 
		   status = HttpStatus.INTERNAL_SERVER_ERROR;
		   labelCode = "Technical problem on serveur : timeout";
	   
	  }
	  if (type.equals(FailureType.SHORTCIRCUIT)) { 
		   status = HttpStatus.TOO_MANY_REQUESTS;
		   labelCode = "Technical problem on serveur : too many requests. The server is shortcircuit";
	  }

	  message = getHystrixRuntimeExceptionMessage(e);
	   
	  
	 
	  logger.error(message, e); 
	 
	  HttpHeaders headers = new HttpHeaders(); 
	  headers.setContentType(MediaType.parseMediaType(MediaTypes.HAL_JSON_VALUE)); 
	  Error error = new Error(ErrorCode.TECHNICAL_ERROR, labelCode, e.getMessage());
	  return new ResponseEntity<Error>(error, status); 
	 }
	
	 
	 
	 @ExceptionHandler(value = { HystrixBadRequestException.class }) 
	 public final ResponseEntity<?> handleException(HystrixBadRequestException e, WebRequest request) { 
	  String message =getHystrixRuntimeExceptionMessage(e);
	  logger.error(message, e); 
	 
	  HttpHeaders headers = new HttpHeaders(); 
	  headers.setContentType(MediaType.parseMediaType(MediaTypes.HAL_JSON_VALUE)); 
	  return handleExceptionInternal(e, message, headers, HttpStatus.BAD_REQUEST, request); 
	 } 
	 
	 private String getHystrixRuntimeExceptionMessage(Throwable e) {
			String message;
			Throwable nestedException = e.getCause();
			   message =  new StringBuilder().append(e.getMessage()).append(" nested exception is ")
			   .append(nestedException.getClass().getName()).append(":").append(nestedException.getMessage())
			   .toString();
			return message;
		} 
	 
	 @ExceptionHandler(value = { InvalidQueryException.class }) 
	 public final ResponseEntity<?> handleInvalidQuery(InvalidQueryException e, WebRequest request) {
		 HttpStatus status = HttpStatus.BAD_REQUEST;
		 HttpHeaders headers = new HttpHeaders(); 
		  headers.setContentType(MediaType.parseMediaType(MediaTypes.HAL_JSON_VALUE)); 
//		  return handleExceptionInternal(e, e.getMessage(), headers, HttpStatus.BAD_REQUEST, request);
		  String labelCode = "invalid query parameters";
		  Error error = new Error(ErrorCode.INVALID_QUERY, labelCode, e.getMessage());
		  return new ResponseEntity<Error>(error, status); 
	
		 
	 }
	}
