package com.orange.lizard.eligibility.resource;

import java.util.HashMap;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.cloud.context.config.annotation.RefreshScope;
import org.springframework.stereotype.Component;

@Component
@RefreshScope
@ConfigurationProperties(prefix="eligibility.commercial")
public class OperationsCommerciales {
	private static final Logger LOGGER = LoggerFactory.getLogger(OperationsCommerciales.class);
	private Map<String,OperationCommerciale> operations= new HashMap<>();
	
	public Map<String, OperationCommerciale> getOperations() {
		return operations;
	}

	public void setOperations(Map<String, OperationCommerciale> operations) {
		this.operations = operations;
	}

}
