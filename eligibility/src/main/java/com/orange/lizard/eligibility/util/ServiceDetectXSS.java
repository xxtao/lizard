package com.orange.lizard.eligibility.util;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.owasp.encoder.Encode;
import org.springframework.stereotype.Service;

import com.orange.lizard.commons.exception.InvalidQueryException;

@Service
public class ServiceDetectXSS {
	
	public ServiceDetectXSS() {
		
	}
	public String detectSpecialCharacter(String queryParameter) {
		Pattern pattern =Pattern.compile("[a-zA-Z]*");
		Matcher m = pattern.matcher (queryParameter); 
		boolean noRisk = m.matches();
		if (!noRisk){
			throw new InvalidQueryException("verify the saisie of channelId");
		}
		String newQueryParameter = Encode.forJava(queryParameter);
		return newQueryParameter;

	}
	
	public String detectOnlyCharacterAndNumber(String queryParameter) {
		Pattern pattern =Pattern.compile("[a-zA-Z_0-9]*");
		Matcher m = pattern.matcher (queryParameter); 
		boolean noRisk = m.matches();
		if (!noRisk){
			throw new InvalidQueryException("verify the saisie of channelId");
		}
		String newQueryParameter = Encode.forJava(queryParameter);
		return newQueryParameter;
	
	}
	
	public String detectInvalidCharacterForCriteria(String queryParameter) {
		
		Pattern pattern =Pattern.compile("[&=[a-zA-Z_0-9]]*");
		Matcher m = pattern.matcher (queryParameter); 
		boolean noRisk = m.matches();
		if (!noRisk){
			throw new InvalidQueryException("verify the saisie of channelId");
		}
		String newQueryParameter = Encode.forJava(queryParameter);
		return newQueryParameter;
		
		
	}
	public String detectInvalidCharacterForCAtegory(String queryParameter) {
		Pattern pattern =Pattern.compile("[&=-_ [a-zA-Z_0-9]]*");
		Matcher m = pattern.matcher (queryParameter); 
		boolean noRisk = m.matches();
		if (!noRisk){
			throw new InvalidQueryException("verify the saisie of channelId");
		}
		String newQueryParameter = Encode.forJava(queryParameter);
		return newQueryParameter;
	
	}
}
