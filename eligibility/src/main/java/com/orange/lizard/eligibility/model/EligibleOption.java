package com.orange.lizard.eligibility.model;

import java.util.ArrayList;
import java.util.List;

import com.orange.lizard.commons.resource.HalResource;

public class EligibleOption extends HalResource {
	
	private String productOfferingId;
	private String productOfferingName;
	private String productId;
	private boolean canBeSubscribed;
	private boolean canBeTerminated;
	private boolean canBeModified;
	private boolean isVisible;
	public boolean isIsVisible() {
		return isVisible;
	}
	public void setIsVisible(boolean isVisible) {
		this.isVisible = isVisible;
	}

	private String type;
	
	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}

	private SubscriptionInformation subscriptionInformation;
	private List<ModifyableFunction> eligibleCharacteristicModifications = new ArrayList<ModifyableFunction>();
	public String getProductOfferingId() {
		return productOfferingId;
	}
	public void setProductOfferingId(String productOfferingId) {
		this.productOfferingId = productOfferingId;
	}
	public String getProductOfferingName() {
		return productOfferingName;
	}
	public void setProductOfferingName(String productOfferingName) {
		this.productOfferingName = productOfferingName;
	}
	public String getProductId() {
		return productId;
	}
	public void setProductId(String productInventoryId) {
		this.productId = productInventoryId;
	}
	public boolean isCanBeSubscribed() {
		return canBeSubscribed;
	}
	public void setCanBeSubscribed(boolean canBeSubscribed) {
		this.canBeSubscribed = canBeSubscribed;
	}
	public boolean isCanBeTerminated() {
		return canBeTerminated;
	}
	public void setCanBeTerminated(boolean canBeTerminated) {
		this.canBeTerminated = canBeTerminated;
	}
	public boolean isCanBeModified() {
		return canBeModified;
	}
	public void setCanBeModified(boolean canBeModified) {
		this.canBeModified = canBeModified;
	}
	public SubscriptionInformation getSubscriptionInformation() {
		return subscriptionInformation;
	}
	public void setSubscriptionInformation(SubscriptionInformation subscriptionInformation) {
		this.subscriptionInformation = subscriptionInformation;
	}
	public List<ModifyableFunction> getEligibleCharacteristicModifications() {
		return eligibleCharacteristicModifications;
	}
	public void setEligibleCharacteristicModifications(List<ModifyableFunction> modificationInformation) {
		this.eligibleCharacteristicModifications = modificationInformation;
	}
	
	public void addEligibleCharacteristicModifications(ModifyableFunction function) {
		this.eligibleCharacteristicModifications.add(function);
	}
	

	private List<MigrateOffers> hasMigrateOffers;
	public List<MigrateOffers> getHasMigrateOffers() {
		return hasMigrateOffers;
	}
	public void setHasMigrateOffers(List<MigrateOffers> hasMigrateOffers) {
		this.hasMigrateOffers = hasMigrateOffers;
	}
}
