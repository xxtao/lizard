package com.orange.lizard.eligibility.util;

import static com.google.common.base.Predicates.or;
import static springfox.documentation.builders.PathSelectors.regex;

import java.lang.reflect.Type;
import java.util.Collection;
import java.util.Date;
import java.util.List;

import javax.xml.datatype.XMLGregorianCalendar;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.domain.Pageable;
import com.google.common.base.Predicate;
import com.fasterxml.classmate.ResolvedType;
import com.fasterxml.classmate.TypeResolver;

import springfox.documentation.builders.ApiInfoBuilder;
import springfox.documentation.schema.AlternateTypeRule;
import springfox.documentation.schema.WildcardType;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.service.Contact;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;

@Configuration
public class SwaggerConfig {

	@Autowired
	  private TypeResolver typeResolver;
	
    @Bean
    public Docket newsApi() {
        return new Docket(DocumentationType.SWAGGER_2)
                .apiInfo(apiInfo())
                .select()
                .paths(paths())
                .build()
                .forCodeGeneration(true)
                .alternateTypeRules(
//                        getAlternateTypeRule(EmbeddedWrapper.class, WildcardType.class, List.class,WildcardType.class),
                        getAlternateTypeRule(Collection.class, WildcardType.class, List.class,WildcardType.class)
                )        
                .directModelSubstitute(XMLGregorianCalendar.class, Date.class)
                .ignoredParameterTypes(Pageable.class)
                ;
                //.additionalModels(typeResolver.resolve(PagedResources.class), typeResolver.resolve(Pageable.class));
    }
     
    private Predicate<String> paths(){
    	return or(
    	        regex("/commercialOperations.*"),
    	        regex("/eligibleOptions.*"),
    	        regex("/eligibleContracts.*")
    			);
    }
    
    
    public AlternateTypeRule getAlternateTypeRule(Type sourceType, Type sourceGenericType,
            Type targetType, Type targetGenericType) {
        TypeResolver typeResolver = new TypeResolver();
        ResolvedType source = typeResolver.resolve(sourceType, sourceGenericType);
        ResolvedType target = typeResolver.resolve(targetType, targetGenericType);
        AlternateTypeRule collectionRule = new AlternateTypeRule(source, target);
        return collectionRule;
    }

    public AlternateTypeRule getAlternateTypeRule(Type sourceType, Type sourceGenericType,
            Type targetType) {
        TypeResolver typeResolver = new TypeResolver();
        ResolvedType source = typeResolver.resolve(sourceType, sourceGenericType);
        ResolvedType target = typeResolver.resolve(targetType);
        AlternateTypeRule collectionRule = new AlternateTypeRule(source, target);
        return collectionRule;
    }    
    /**
     * API Info in swagger json/yml doesn't generate any Java artifact 
     * @return Informations concerning API.
     */
    private ApiInfo apiInfo() {
    	
        return new ApiInfoBuilder()
                .title("Lizard Eligibility API")
                .contact(new Contact("Orange-ISAD","",""))
                .version("0.1")
                .build();
    }

}
