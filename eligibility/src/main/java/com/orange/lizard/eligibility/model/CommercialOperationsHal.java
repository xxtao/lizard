package com.orange.lizard.eligibility.model;

import java.util.ArrayList;
import java.util.List;

import javax.xml.datatype.XMLGregorianCalendar;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.orange.lizard.commons.resource.HalResource;

public class CommercialOperationsHal extends HalResource {
	
	public String productId;
	public List<OperationCommercialeAuthorized> commercialOperations = new ArrayList<OperationCommercialeAuthorized>();
	private XMLGregorianCalendar operationDate;
	
	@JsonProperty("commercialOperations")
	public List<OperationCommercialeAuthorized> getCommercialOperations() {
		return commercialOperations;
	}
	public void setCommercialOperations(List<OperationCommercialeAuthorized> commercialOperations) {
		this.commercialOperations = commercialOperations;
	}
	
	@JsonProperty("operationDate")
	public XMLGregorianCalendar getOperationDate() {
		return operationDate;
	}
	public void setOperationDate(XMLGregorianCalendar operationDate) {
		this.operationDate = operationDate;
	}
	
	

	@JsonProperty("productId")
	public String getProductInventoryId() {
		return productId;
	}
	public void setProductInventoryId(String productId) {
		this.productId = productId;
	}

}
