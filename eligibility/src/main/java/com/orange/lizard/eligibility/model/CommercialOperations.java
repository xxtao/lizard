package com.orange.lizard.eligibility.model;

import java.util.ArrayList;
import java.util.List;

import javax.xml.datatype.XMLGregorianCalendar;

public class CommercialOperations {
	
	private String productID;
	private List<OperationCommercialeAuthorized> commercialOperations = new ArrayList<OperationCommercialeAuthorized>();
	private XMLGregorianCalendar operationDate;
	public String getProductInvotenryId() {
		return productID;
	}
	public void setProductInvotenryId(String productID) {
		this.productID = productID;
	}
	public List<OperationCommercialeAuthorized> getCommercialOperations() {
		return commercialOperations;
	}
	public void setCommercialOperations(List<OperationCommercialeAuthorized> commercialOperations) {
		this.commercialOperations = commercialOperations;
	}
	public XMLGregorianCalendar getOperationDate() {
		return operationDate;
	}
	public void setOperationDate(XMLGregorianCalendar operationDate) {
		this.operationDate = operationDate;
	}
	
	
	
	

}
