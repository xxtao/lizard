package com.orange.lizard.eligibility.util;

import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;


@Configuration
@EnableWebSecurity
public class WebSecurityConfig extends WebSecurityConfigurerAdapter {
	
	
    @Override
    protected void configure(HttpSecurity http) throws Exception {
        http
	        .authorizeRequests()
	        	.anyRequest().permitAll()
	        	.and()
	        .formLogin()
	        	.and()
	        .httpBasic().disable()
    	
	        .headers()
				.xssProtection()
					.block(true);
    }

}
