package com.orange.lizard.eligibility.model;

public class Amount {
	
	private String id;
	private String name;
	private double taxIncludedAmount;
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public double getTaxIncludedAmount() {
		return taxIncludedAmount;
	}
	public void setTaxIncludedAmount(double amountIncudingTax) {
		this.taxIncludedAmount = amountIncudingTax;
	}
	
	

}
