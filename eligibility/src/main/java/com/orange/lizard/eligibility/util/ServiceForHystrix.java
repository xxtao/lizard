package com.orange.lizard.eligibility.util;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.cloud.netflix.hystrix.EnableHystrix;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import com.francetelecom.soft.kernel.model.catalogue.Catalogue;
import com.francetelecom.soft.kernel.model.catalogue.Classification;
import com.francetelecom.soft.kernel.model.catalogue.ContractTypeSpecification;
import com.francetelecom.soft.kernel.model.catalogue.OfferSpecification;
import com.francetelecom.soft.kernel.model.customerorder.CustomerOrder;
import com.francetelecom.soft.kernel.model.customerorder.CustomerOrderCompositeItem;
import com.francetelecom.soft.kernel.model.customerorder.CustomerOrderItem;
import com.francetelecom.soft.kernel.model.installedbase.InstalledCompositeOffer;
import com.francetelecom.soft.kernel.model.installedbase.InstalledOffer;
import com.francetelecom.soft.kernel.model.util.StringUtils;
import com.francetelecom.soft.kernel.service.catalogue.CatalogueService;
import com.francetelecom.soft.kernel.service.configurator.ConfiguratorService;
import com.francetelecom.soft.kernel.service.configurator.ConfiguratorServiceImpl;
import com.francetelecom.soft.kernel.service.eligibility.ContractAcquisitionEligibilityRequest;
import com.francetelecom.soft.kernel.service.eligibility.EligibilityRequestContext;
import com.francetelecom.soft.kernel.service.eligibility.EligibilityService;
import com.francetelecom.soft.kernel.service.eligibility.EligibleOffer;
import com.francetelecom.soft.kernel.service.eligibility.EligibleOffers;
import com.francetelecom.soft.kernel.service.eligibility.MigrationEligibilityRequest;
import com.netflix.hystrix.contrib.javanica.annotation.HystrixCommand;
import com.netflix.hystrix.contrib.javanica.annotation.HystrixProperty;
import com.orange.lizard.commons.service.InstalledBaseService;
import com.orange.lizard.eligibility.model.CategoryIcon;
import com.orange.lizard.eligibility.model.ProductOfferingIcon;

@Service
//@EnableCircuitBreaker
@EnableHystrix
public class ServiceForHystrix {
	private static final Logger LOGGER = LoggerFactory.getLogger(ServiceForHystrix.class);
	
	 @Value("${lizard.service.productinventory.url}")
	 private String urProductInventoryAPI;
	
	@Autowired
	private EligibilityService eligibilityService;
		
	@Autowired
	private CatalogueService catalogueService;

	@Autowired
	private InstalledBaseService installedBaseService;
	
	private boolean isMigrationOder = false;

	
	protected Catalogue getCatalogue() {
		// Warn, should use a date from the order for this!
		return catalogueService.getCatalogue();
	}
	

	
	
	@HystrixCommand
	(commandProperties = { @HystrixProperty (name = "execution.isolation.thread.timeoutInMilliseconds",value = "8000") })
	public Page<ProductOfferingIcon> getAllContracts(String channelId,String salesPoint,String productCategory,String partyId,String productInventoryId,String productOfferingId, String eligibleCriterias) {
		List<ProductOfferingIcon> res = getAllContractsWithoutPage(channelId, productCategory, partyId,
				productInventoryId, productOfferingId, eligibleCriterias);
		// BIAF7492 : 21/06/2017
		Pageable pageable = new PageRequest(0,res.size());
		Page<ProductOfferingIcon> result = new PageImpl<>(res, pageable, res.size());

		isMigrationOder = false;
		return result;

	}

	public List<ProductOfferingIcon> getAllContractsWithoutPage(String channelId, String productCategory,
			String partyId, String productInventoryId, String productOfferingId, String eligibleCriterias) {
		Catalogue catalogue = getCatalogue();
		
		CustomerOrder customerOrder = new CustomerOrder();
		ConfiguratorService configuratorService = new ConfiguratorServiceImpl();
		configuratorService.setCatalogueService(catalogueService);
		
		Date virtualDate = Calendar.getInstance().getTime();
		if (virtualDate != null) {
			customerOrder.setCreationDate(virtualDate);
			customerOrder.setConfigurationDate(virtualDate);

		} else {
			customerOrder.setConfigurationDate(catalogue.getEffectiveDate());
		}

		customerOrder.setCatalogueVersion(catalogue.getCastorVersion());
		customerOrder.setDistributorChannelId(channelId);
		customerOrder.setId(partyId);

		customerOrder.setDoManagePendingInstalledStatus(true);
		if (StringUtils.isNotEmpty(eligibleCriterias)){
			Map<String,String> criterias = decodeCriterias(eligibleCriterias);
			
			for (Map.Entry<String,String> entry : criterias.entrySet()) {
				String key=entry.getKey();
				String value = entry.getValue();
				customerOrder.setConfigurationContextValue(key, value);
			}
		}

		List<ProductOfferingIcon> res = new ArrayList<>();

		List<EligibleOffer> listOfEligibleOffersForCategory = new ArrayList<>();

		if (  StringUtils.isEmpty(productInventoryId) && ( productOfferingId == null || productOfferingId.equalsIgnoreCase("") ) ){
			isMigrationOder = false;

		}else {
			isMigrationOder = true;
		}

		if (isMigrationOder) {
			String idContractCatalog ;
			if ( StringUtils.isNotEmpty(productOfferingId)  ) {
				idContractCatalog = productOfferingId;
			}else {
				idContractCatalog = findProductIdCatalog(productInventoryId);
			}
			 
			
			Collection<CustomerOrderItem> customerOrderItems = new ArrayList<>();
			CustomerOrderItem contractCOI = new CustomerOrderCompositeItem();

			customerOrderItems.add(contractCOI);
			List<InstalledOffer> listInstalledOffer = new ArrayList<>();
			InstalledOffer installedOffer = new InstalledCompositeOffer();
			ContractTypeSpecification contractTypeSpecification = new ContractTypeSpecification();

			contractTypeSpecification.setCode(idContractCatalog);

			((InstalledCompositeOffer) installedOffer).setCompositeOffer(contractTypeSpecification);

			listInstalledOffer.add(installedOffer);

			customerOrder.addCustomerOrderItem(contractCOI);

			MigrationEligibilityRequest migRequest = MigrationEligibilityRequest.build()
					.withContext(EligibilityRequestContext.build(customerOrder).create())
					.withSourceInstalledOffers(listInstalledOffer).create();

			EligibleOffers foundOffers = eligibilityService.getEligibleOffersForMigration(migRequest);
			List<EligibleOffer> listOfEligibleOffers = foundOffers.getEligibleOffers();
			for (EligibleOffer eligibleOffer : listOfEligibleOffers) {
				
				OfferSpecification offerSpecification = eligibleOffer.getOfferSpecification();
				String genericProduct = offerSpecification.getGenericProduct();
				String idProduct = offerSpecification.getCode();
				String nameProduct = offerSpecification.getLabel();
				

				Collection<Classification> classifications = offerSpecification.getClassifications();
				// **
				ProductOfferingIcon product = new ProductOfferingIcon();
				buildProduct(product, idProduct,genericProduct,nameProduct);
				List<CategoryIcon> classifIcon = new ArrayList<>();
				for (Classification classif : classifications) {
					CategoryIcon cat = new CategoryIcon();
//					ProductOfferingIcon product = new ProductOfferingIcon();
//					buildProduct(product, idProduct,genericProduct,nameProduct);
					cat = new CategoryIcon();
					cat.setId(classif.getCode());
					cat.setName(classif.getLabel());
					classifIcon.add(cat);
					
					
				}
				product.setCategories(classifIcon);

				res.add(product);

				// **

			}
		} else {  //acquistion 
			EligibleOffers foundOffers;

			// Collection<CustomerOrderItem> customerOrderItems =
			// customerOrder.getCustomerOrderItems();

			ContractAcquisitionEligibilityRequest acqRequest = ContractAcquisitionEligibilityRequest.build()
					.withContext(EligibilityRequestContext.build(customerOrder).create()).create();
			foundOffers = eligibilityService.getEligibleContractsForAcquisition(acqRequest);

			List<EligibleOffer> listOfEligibleOffers = foundOffers.getEligibleOffers();

			for (EligibleOffer eligibleOffer : listOfEligibleOffers) {
				
				OfferSpecification offerSpecification = eligibleOffer.getOfferSpecification();
				String genericProduct = offerSpecification.getGenericProduct();
				String idProduct = offerSpecification.getCode();
				String nameProduct = offerSpecification.getLabel();

				
		

				Collection<Classification> classifications = offerSpecification.getClassifications();

				CategoryIcon cat;
				ProductOfferingIcon product = new ProductOfferingIcon();
				buildProduct(product, idProduct,genericProduct,nameProduct);
				List<CategoryIcon> classifIcon = new ArrayList<>();
				for (Classification classif : classifications) {
					cat = new CategoryIcon();
					
					cat.setId(classif.getCode());
					cat.setName(classif.getLabel());
					
					
					if (!StringUtils.isEmpty(productCategory)&&(classif.getCode().equalsIgnoreCase(productCategory)))
							 {
						listOfEligibleOffersForCategory.add(eligibleOffer);
						classifIcon.add(cat);
						//res.add(product);
						//} else if (productCategory.equalsIgnoreCase("")) {
					} else if (productCategory==null) {
						classifIcon.add(cat);
					}

				}
				
				product.setCategories(classifIcon);
				res.add(product);
				

			}
		}
		return res;
	}

	public void buildProduct(ProductOfferingIcon product,String id, String genericProduct ,String nameProduct) {
		product.setGenericProduct(genericProduct);

		product.setId(id);

		product.setName(nameProduct);
	}

	private String findProductIdCatalog(String productInventoryId) {
		
		try {
			InstalledOffer io= installedBaseService.getInstalledOfferById(productInventoryId);
			if(null!=io){
				return io.getProductCode();
			}
		
//			String productOfferingId = null ;
//			RestTemplate restTemplate = new RestTemplate();
//			restTemplate.setInterceptors(Arrays.asList(new CallInterceptor()));
//			
//			if(!urProductInventoryAPI.endsWith("/")){
//				urProductInventoryAPI+="/";
//			}
//			String url = urProductInventoryAPI +productInventoryId+"?:fields=all";
//			resp = restTemplate.getForObject(
//					url,
//					Products.class);
//			List<Product> listOfProduct = resp.getProduct();
//			for (Product product : listOfProduct) {
//				if (product.getId().equalsIgnoreCase(productInventoryId)) {
//					productOfferingId =product.getProductOffering().getId();
//				}
//			}
		}catch (Exception e){
			LOGGER.error("Err calling productInventory",e);
		}
		

		return null;
	}
	
	private Map<String, String> decodeCriterias(String eligibleCriterias) {
		Map<String, String> criterias = new HashMap<>();
		String[] keyValues = eligibleCriterias.split("&");
		for (String eachKeyValue : keyValues){
			String[] keyValue = eachKeyValue.split("=");
			 String key = keyValue [0];
			 String value = keyValue [1];
			 criterias.put(key, value);
			 
		}
		return criterias;
	}

	}


