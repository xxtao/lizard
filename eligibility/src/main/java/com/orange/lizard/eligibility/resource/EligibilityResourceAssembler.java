/**
 * 
 */
package com.orange.lizard.eligibility.resource;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.hateoas.Link;
import org.springframework.hateoas.mvc.ResourceAssemblerSupport;
import org.springframework.stereotype.Service;

import com.orange.lizard.eligibility.model.Category;
import com.orange.lizard.eligibility.model.CategoryIcon;
import com.orange.lizard.eligibility.model.ProductOffering;
import com.orange.lizard.eligibility.model.ProductOfferingIcon;

/**
 * @author amcu6536
 *
 */
@Service
public class EligibilityResourceAssembler extends ResourceAssemblerSupport<ProductOfferingIcon, ProductOffering> {
	
	 @Value("${lizard.service.productinventory.link.category.url}")
	 private String urlProductInventoryCategoryAPILink;
	 
	 @Value("${lizard.service.productinventory.link.productOfferings.url}")
	 private String urlProductInventoryAPILink;
	 
	

	public EligibilityResourceAssembler() {
		this(EligibiltyController.class, ProductOffering.class);
	}

	public EligibilityResourceAssembler(Class<?> controllerClass, Class<ProductOffering> resourceType) {
		super(controllerClass, resourceType);
	}

	
	public List<ProductOffering> toResourceList(List<ProductOfferingIcon> productsEligible) {

		List<ProductOffering> resources = new ArrayList<>(productsEligible.size());
		
		for (ProductOfferingIcon productOffering : productsEligible) {
		ProductOffering resource = toResource(productOffering);
		resources.add(resource);
		}
		
		return resources;
		
	}

	@Override
	public ProductOffering toResource(ProductOfferingIcon productOffering) {
		ProductOffering res = new ProductOffering();
		
		res.setIdent(productOffering.getId());
		res.setGenericProduct(productOffering.getGenericProduct());
		res.setName(productOffering.getName());
		res.add(new Link(urlProductInventoryAPILink + productOffering.getId(), "self"));
		List<CategoryIcon> listCatIcon =  productOffering.getCategories();
		List<Category> listCat =  new ArrayList<Category>();
		for (CategoryIcon catIcon : listCatIcon) {
			
		
			if (catIcon != null) {
				Category cat = new Category();
				cat.setIdent(catIcon.getId());
				cat.setName(catIcon.getName());
				cat.add(new Link(urlProductInventoryCategoryAPILink + cat.getIdent(), "self"));
				listCat.add(cat);
						
			}
			
		}
		res.setCategories(listCat);
		return res;
	}

}
