package com.orange.lizard.eligibility;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.cloud.netflix.feign.EnableFeignClients;
import org.springframework.cloud.netflix.hystrix.EnableHystrix;
import org.springframework.cloud.sleuth.sampler.AlwaysSampler;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.ImportResource;

import feign.Logger;
import springfox.documentation.swagger2.annotations.EnableSwagger2;


@Configuration
@ComponentScan
@EnableAutoConfiguration // C'est un client d'un SpringCloud ServerConfig potentiel
@ImportResource("classpath:applicationContext.xml")
@EnableFeignClients
@EnableSwagger2
@EnableHystrix
@EnableDiscoveryClient
public class LizardEligibilityApplication {
	
	public static void main(String[] args) {
		
		ConfigurableApplicationContext ctx = null;
		
		try {
			ctx = SpringApplication.run(LizardEligibilityApplication.class, args);
		} catch (Exception e) {
			
			if (ctx != null) {
				ctx.close();
			}
		}

	}
	
	@Bean
	public AlwaysSampler defaultSampler() {
		return new AlwaysSampler();
	}
	
	@Bean
    Logger.Level feignLoggerLevel() {
        return Logger.Level.FULL;
    }
	
}



	
