package com.orange.lizard.eligibility.resource;

import java.util.List;

public class OperationCommerciale {

	private String libelle;
	private List<String> detail;

	public String getLibelle() {
		return libelle;
	}

	public void setLibelle(String libelle) {
		this.libelle = libelle;
	}

	public List<String> getDetail() {
		return detail;
	}

	public void setDetail(List<String> detail) {
		this.detail = detail;
	}

}
