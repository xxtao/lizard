package com.orange.lizard.eligibility.model;

import java.util.ArrayList;
import java.util.List;

public class SubscriptionInformation {
	
	private List<EligibilityAlert> eligibilityAlerts = new ArrayList<EligibilityAlert>();
	private List<Tariff> eligibleOptionPrices = new ArrayList<Tariff>();
	
	public List<EligibilityAlert> getEligibilityAlerts() {
		return eligibilityAlerts;
	}
	public void setEligibilityAlerts(List<EligibilityAlert> eligibilityAlerts) {
		this.eligibilityAlerts = eligibilityAlerts;
	}
	public void addEligibilityAlert(EligibilityAlert alert) {
		this.eligibilityAlerts.add(alert);
	}
	public List<Tariff> getEligibleOptionPrices() {
		return eligibleOptionPrices;
	}
	public void setEligibleOptionPrices(List<Tariff> tariffs) {
		this.eligibleOptionPrices = tariffs;
	}
	public void addEligibleOptionPrices(Tariff tariff) {
		this.eligibleOptionPrices.add(tariff);
	}
	
	

}
