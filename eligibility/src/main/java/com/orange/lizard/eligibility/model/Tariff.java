package com.orange.lizard.eligibility.model;

import java.util.ArrayList;
import java.util.List;

public class Tariff extends Amount {
	
	private List<Amount> discounts = new ArrayList<Amount>();

	public List<Amount> getDiscounts() {
		return discounts;
	}

	public void setDiscounts(List<Amount> discounts) {
		this.discounts = discounts;
	}
	
	public void addDiscount(Amount discount) {
		this.discounts.add(discount);
	}

}
