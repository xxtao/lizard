package com.orange.lizard.eligibility.model;

public class FunctionValue {
	
	private String systemValueCode;
	private String value;
	public String getSystemValueCode() {
		return systemValueCode;
	}
	public void setSystemValueCode(String systemValueCode) {
		this.systemValueCode = systemValueCode;
	}
	public String getValue() {
		return value;
	}
	public void setValue(String value) {
		this.value = value;
	}
	
	

}
