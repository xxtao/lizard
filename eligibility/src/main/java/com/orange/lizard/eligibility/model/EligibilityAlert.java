package com.orange.lizard.eligibility.model;

public class EligibilityAlert {
	
	private String eligibilityAlertGravity;
	private String eligibilityAlertCode;
	private String eligibilityAlertLabel;
	public String getEligibilityAlertGravity() {
		return eligibilityAlertGravity;
	}
	public void setEligibilityAlertGravity(String eligibilityAlertGravity) {
		this.eligibilityAlertGravity = eligibilityAlertGravity;
	}
	public String getEligibilityAlertCode() {
		return eligibilityAlertCode;
	}
	public void setEligibilityAlertCode(String eligibilityAlertCode) {
		this.eligibilityAlertCode = eligibilityAlertCode;
	}
	public String getEligibilityAlertLabel() {
		return eligibilityAlertLabel;
	}
	public void setEligibilityAlertLabel(String eligibilityAlertLabel) {
		this.eligibilityAlertLabel = eligibilityAlertLabel;
	}
	
	

}
