package com.orange.lizard.eligibility.util;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Service;

import com.francetelecom.onecrm.configurator.core.configuration.sessionview.ContractSessionView;
import com.francetelecom.onecrm.configurator.core.configuration.sessionview.MigrationOffer;
import com.francetelecom.onecrm.configurator.core.configuration.sessionview.OfferSessionView;
import com.francetelecom.soft.kernel.model.configurator.Option;
import com.francetelecom.soft.kernel.model.configurator.Option.OptionType;
import com.francetelecom.soft.kernel.model.configurator.OptionDiscount;
import com.francetelecom.soft.kernel.model.configurator.OptionError;
import com.francetelecom.soft.kernel.model.configurator.OptionError.Level;
import com.francetelecom.soft.kernel.model.configurator.OptionErrorType;
import com.francetelecom.soft.kernel.model.configurator.OptionFunction;
import com.francetelecom.soft.kernel.model.configurator.OptionFunctionValue;
import com.francetelecom.soft.kernel.model.configurator.OptionTariff;
import com.francetelecom.soft.kernel.model.configurator.helper.OptionHelper;
import com.francetelecom.soft.kernel.model.configurator.helper.OptionVisitor;
import com.orange.lizard.eligibility.model.Amount;
import com.orange.lizard.eligibility.model.EligibilityAlert;
import com.orange.lizard.eligibility.model.EligibleOption;
import com.orange.lizard.eligibility.model.FunctionValue;
import com.orange.lizard.eligibility.model.MigrateOffers;
import com.orange.lizard.eligibility.model.ModifyableFunction;
import com.orange.lizard.eligibility.model.SubscriptionInformation;
import com.orange.lizard.eligibility.model.Tariff;

@Service
public class EligibleOptionFactory {
	
	
	public List<EligibleOption> buildEligibleOptions(final Option root, ContractSessionView contractSessionView) {
		List<EligibleOption> eligibleOptions = new ArrayList<EligibleOption>();
		OptionHelper.visitHierarchy(root, new OptionVisitor() {
			public Result visit(Option option) {
				EligibleOption eo = new EligibleOption();
				SubscriptionInformation info = new SubscriptionInformation();
				eo.setSubscriptionInformation(info);
				
//				List<OptionCommercialParameter> listocp = option.getCommercialParameters();
//				List<OptionLink> listol = option.getLinks();
//				option.getCommercialParameters();
//				List<OptionAction> list = option.getActions();
//				List<OptionCommercialParameter> listco = option.getCommercialParameters();
//				for(OptionCommercialParameter oa:listco){
//					System.out.println(oa.toString());
//				}
						
				eo.setProductOfferingId(option.getOfferCode());
				eo.setProductOfferingName(option.getLabel());
				eo.setProductId(option.getInstalledId());
				eo.setType(option.getOptionType().name());
				eligibleOptions.add(eo);
//				if (option.isSelected() && contractSessionView != null && option.getOptionType().equals(OptionType.atomicOffer)) {
				if (option.isSelected() && contractSessionView != null && !(option.getOptionType().equals(OptionType.contract))) {
//					if (option.getOptionType().equals(OptionType.atomicOffer)) {
						OfferSessionView offerSessionView = contractSessionView.getOfferView(option.getOfferCode());
						List<MigrationOffer> listMigrationOffers = offerSessionView.getMigrationOffers();
						List<MigrateOffers> listMigrateOffers = new ArrayList<MigrateOffers>();
						if (listMigrationOffers !=null && !listMigrationOffers.isEmpty() ){
							
							for (MigrationOffer mig:listMigrationOffers ){
								MigrateOffers migrateOffers = new MigrateOffers();
//								migrateOffers.setCommercialVarations(mig.getCommercialVariations());
								migrateOffers.setProductOfferingId( mig.getOfferCode());
								listMigrateOffers.add(migrateOffers);
							}
							
							eo.setHasMigrateOffers(listMigrateOffers);
//						}
					}					
				}
				if (option.isSelectable() && !option.isSelected()) {
					boolean canBeSubscribed = true;
					if (option.getErrors() != null) {
						for(OptionError error : option.getErrors()) {
							if(error.getLevel() == Level.ERROR &&
									(error.getType() == OptionErrorType.incompatibility ||
									error.getType() == OptionErrorType.need ||
									error.getType() == OptionErrorType.commercialHierarchy ||
									error.getType() == OptionErrorType.custom)) {
								canBeSubscribed = false;
								break;
							}
						}
					}
					eo.setCanBeSubscribed(canBeSubscribed);
					if (canBeSubscribed){
						if (option.getTariffs() != null && !option.getTariffs().isEmpty()) {
							for (OptionTariff ot : option.getTariffs()) {
								Tariff tariff = new Tariff();
								tariff.setId(ot.getCode());
								tariff.setName(ot.getLabel());
								tariff.setTaxIncludedAmount(ot.getAmountIncludingTax());
								if (ot.getDiscounts() != null) {
									for (OptionDiscount od : ot.getDiscounts()) {
										Amount discount = new Amount();
										discount.setId(od.getCode());
										discount.setName(od.getLabel());
										discount.setTaxIncludedAmount(od.getAmountIncludingTax());
										tariff.addDiscount(discount);
									}
								}
								info.addEligibleOptionPrices(tariff);
							}
						}
					}
				} 
				
				if (option.isSelectable() && option.isSelected()) {
					eo.setCanBeTerminated(true);

				}
				
				if (option.isSelected() && option.getFunctions() != null && !option.getFunctions().isEmpty()) {
					eo.setCanBeModified(true);
					for (OptionFunction funct : option.getFunctions()) {
						ModifyableFunction function = new ModifyableFunction();
						function.setId(funct.getCode());
						function.setFunctionLabel(funct.getLabel());
						
						if (!StringUtils.isEmpty(funct.getInstalledValue()) && funct.isModifiable()) {
							
							if (funct.getSelectableValues() !=null && funct.getSelectableValues().isEmpty()) {
								function.setValue(funct.getInstalledValue());
//								function.setSystemValueCode(funct.get));
							} else {
								for (OptionFunctionValue value : funct.getSelectableValues()) {
									if (funct.getInstalledValue() != value.getCode()) {
										FunctionValue functValue = new FunctionValue();
										functValue.setSystemValueCode(value.getCode());
										functValue.setValue(value.getLabel());
										function.addProductSpecCharacteristicValue(functValue);
									} else {
										function.setValue(funct.getInstalledValue());
//										function.setInstalledFunctionValueLabel(value.getLabel());
									}
								}
							}
						}
						eo.addEligibleCharacteristicModifications(function);

					}
				}
				if (option.getErrors() != null) {
					for(OptionError error : option.getErrors()) {
						if(error.getType() == OptionErrorType.incompatibility ||
								error.getType() == OptionErrorType.need ||
								error.getType() == OptionErrorType.commercialHierarchy ||
								error.getType() == OptionErrorType.custom) {
							EligibilityAlert alert = new EligibilityAlert();
							alert.setEligibilityAlertCode(error.getCode());
							alert.setEligibilityAlertGravity(error.getLevel().getCode());
							alert.setEligibilityAlertLabel(error.getMessage());
							info.addEligibilityAlert(alert);
						}
					}
				}
				return Result.CONTINUE;
			}
		});
		
		return eligibleOptions;
	}

	public Map<String, String> decodeCriterias(String eligibleCriterias) {
		Map<String, String> criterias = new HashMap<String, String>();
//		String[] keyValues = eligibleCriterias.split("%26");
		String[] keyValues = eligibleCriterias.split("&");
		for (String eachKeyValue : keyValues){
//			 String[] keyValue = eachKeyValue.split("%3D");
			String[] keyValue = eachKeyValue.split("=");
			 String key = keyValue [0];
			 String value = keyValue [1];
			 criterias.put(key, value);
			 
		}
		return criterias;
	}

}
