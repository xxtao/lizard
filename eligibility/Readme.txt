Exemple de lancement sous eclipse sans serverConfig (git) et en changeant de contextPath:
arguments : --spring.profiles.active=native --spring.cloud.config.server.native.searchLocations=file:///D:/java/workspaces42/lizard/api-configuration   --server.port=8083 --server.context-path=/eligibility


exemple de fichier lizard-api-eligibility.properties: 
-----------------------------------------------------
operations[ACQ].libelle = Acquisition de nouveau contrat
operations[ACQ].detail = SHOP,CallCenter
operations[MIG].libelle = Migration de contrat
operations[MIG].detail = SHOP,CallCenter
operations[MOD].libelle = Ajout/suppression/Modif des options d\u2019un contrat d\u00e9tenu (modification de parc)
operations[MOD].detail = WEB,SHOP,CallCenter,USSD
operations[TER].libelle = R\u00e9silier un contrat
operations[TER].detail = SHOP,CallCenter
operations[REC].libelle = Ajout de recharge
operations[REC].detail = WEB,SHOP,CallCenter,USSD
operations[SIM].libelle = SIMSwap
operations[SIM].detail = WEB,SHOP,CallCenter
operations[DEV].libelle = ajout de device
operations[DEV].detail = WEB,SHOP,CallCenter
operations[SUS].libelle = suspendre le contrat
operations[SUS].detail = SHOP,CallCenter
operations[REA].libelle = r\u00e9activer un contrat
operations[REA].detail = WEB,SHOP,CallCenter,USSD
operations[OWN].libelle = Changer le d\u00e9tenteur
operations[OWN].detail = SHOP
operations[MOV].libelle = g\u00e9rer le d\u00e9m\u00e9nagement
operations[MOV].detail = SHOP
operations[OMY].libelle = souscription Orange Money
operations[OMY].detail = SHOP,CallCenter
-----------------------------------------------------

